import json
import sys

import xlrd


workbook = xlrd.open_workbook("https://dev-skandha-vod.s3.ap-south-1.amazonaws.com/input-data/")
worksheet = workbook.sheet_by_name("WellNessSample.xlsx")

data = []
keys = [v.value for v in worksheet.row(0)]
for row_number in range(worksheet.nrows):
    if row_number == 0:
        continue
    row_data = {}
    for col_number, cell in enumerate(worksheet.row(row_number)):
        row_data[keys[col_number]] = cell.value
    data.append(row_data)

with open('asdfghj', 'w') as json_file:
    json_file.write(json.dumps({'data': data}))