const airfileData = require('../app/models/airFileData');


class airfileDataCls {
    airfileDataCls() { }


    
    save(params) {
        const fileDocument = new airfileData({
            playlistfileName: params.playlistfileName ? params.playlistfileName : null,
            airfileName: params.airfileName ? params.airfileName : null,
            airfileData : params.airfileData ? params.airfileData : null
        });
        return new Promise((resolve, reject) => {
            fileDocument.save((err, data) => {
                if (err) {
                    console.error(`Error :: Mongo fileDocument Save Error :: ${JSON.stringify(err)}`);
                    reject(err);
                } else {
                    const response = {
                        code: 200,
                        message: data,
                    };
                    resolve(response);
                }
            });
        });
    }




    



    // Find fileData By File Name 
    findAirFilebyFileName(params) {

        console.log("params workers"+JSON.stringify(params))
        return new Promise((resolve, reject) => {
            airfileData.findOne({ playlistfileName: params.playlistFilename })
                .exec((err, data) => {
                    if (err) {
                        console.error(`Error :: findAirFilebyFileName :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {

                        resolve(data);
                       
                    }
                });
        })
    }

}


module.exports = {
    airfileDataClass: airfileDataCls,
};