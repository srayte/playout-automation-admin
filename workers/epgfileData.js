const epgfileData = require('../app/models/epgdata');


class epgfileDataCls {
    epgfileDataCls() { }


    
    save(params) {
        const fileDocument = new epgfileData({
            fileName: params.fileName ? params.fileName : null,
            fileData: params.fileData ? params.fileData : null,
            UserName: params.UserName ? params.UserName : null,
            playlistFilename : params.playlistFilename ? params.playlistFilename : null
        });
        return new Promise((resolve, reject) => {
            fileDocument.save((err, data) => {
                if (err) {
                    console.error(`Error :: Mongo fileDocument Save Error :: ${JSON.stringify(err)}`);
                    reject(err);
                } else {
                    const response = {
                        code: 200,
                        message: data,
                    };
                    resolve(response);
                }
            });
        });
    }




    



    // Find fileData By File Name 
    findEPGbyFileName(params) {
        return new Promise((resolve, reject) => {
            epgfileData.findOne({ playlistFilename: params.playlistFilename })
                .exec((err, data) => {
                    if (err) {
                        console.error(`Error :: findEPGbyFileName :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {

                        resolve(data);
                       
                    }
                });
        })
    }

}


module.exports = {
    epgfileDataClass: epgfileDataCls,
};