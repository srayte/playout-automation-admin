const harekrishnaData = require('../app/models/harekrishnaData');


class harekrishnaDataCls {
    harekrishnaDataCls() { }

    save(params) {
        const fileDocument = new harekrishnaData({
            fileName: params.fileName ? params.fileName : null,
            fileData: params.fileData ? params.fileData : null,
            UserName: params.UserName ? params.UserName : null,
            ChannelName: params.channelName ? params.channelName : null,
        });
        return new Promise((resolve, reject) => {
            fileDocument.save((err, data) => {
                if (err) {
                    console.error(`Error :: Mongo fileDocument Save Error :: ${JSON.stringify(err)}`);
                    reject(err);
                } else {
                    const response = {
                        code: 200,
                        message: data,
                    };
                    resolve(response);
                }
            });
        });
    }

    findFileDataByUserName(params) {
        return new Promise((resolve, reject) => {
            harekrishnaData.find({fileName: "HareNew.xlsx"} ,{fileData: {$elemMatch: {ID: params.ID}}})
                .exec((err, data) => {
                    if (err) {
                        console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                       // console.log("HareNew.xlsx  data  >>"+JSON.stringify(data))
                        resolve(data);
                        
                    }
                });
        })
    }


     // Get All Data 
    findall() {
        return new Promise((resolve, reject) => {
            harekrishnaData.find({})
                .sort({ created_at: -1 })
                .exec((err, data) => {
                    if (err) {
                        console.error(`Error :: Mongo Find all data has error :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
        });
    }



   
     // Delete Channel
     deleteFile(params) {
        return new Promise((resolve, reject) => {
            const obj = {};
            harekrishnaData.deleteOne({ fileName: params.File_name })
                .exec((err, data) => {
                    if (err) {
                        reject(err);
                    } else {
                        console.log(`DELETE Channel SUCCESS : ${JSON.stringify(data)}`);
                        obj.status = 200;
                        obj.data = data;
                        resolve(obj);
                    }
                });
        });
    }





}

module.exports = {
    harekrishnaDataClass: harekrishnaDataCls,
};