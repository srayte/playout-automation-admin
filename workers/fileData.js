const fileData = require("../app/models/fileData");

class fileDataCls {
  fileDataCls() {}

  save(params) {
    const fileDocument = new fileData({
      fileName: params.fileName ? params.fileName : null,
      fileData: params.fileData ? params.fileData : [],
      UserName: params.UserName ? params.UserName : null,
    });
    return new Promise((resolve, reject) => {
      fileDocument.save((err, data) => {
        if (err) {
          console.error(
            `Error :: Mongo fileDocument Save Error :: ${JSON.stringify(err)}`
          );
          reject(err);
        } else {
          const response = {
            code: 200,
            message: data,
          };
          resolve(response);
        }
      });
    });
  }

  // Find fileData By UserName
  findFileDataByUserid(params) {
    return new Promise((resolve, reject) => {
      fileData.find({ UserName: params.UserName }).exec((err, data) => {
        if (err) {
          console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
          reject(err);
        } else {
          resolve(data);
        }
      });
    });
  }

  // Find fileData By File Name
  findFileDataByFileName(params) {
    return new Promise((resolve, reject) => {
      fileData.findOne({ fileName: params.fileName }).exec((err, data) => {
        if (err) {
          console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
          reject(err);
        } else {
          resolve(data);
        }
      });
    });
  }

  // Find fileData By File Name
  findEPGbyFileName(params) {
    return new Promise((resolve, reject) => {
      fileData.findOne({ fileName: params.fileName }).exec((err, data) => {
        if (err) {
          console.error(`Error :: findEPGbyFileName :: ${JSON.stringify(err)}`);
          reject(err);
        } else {
          resolve(data);
        }
      });
    });
  }

  // srmd Update in fileData
  findOneAndUpdate(params) {
    return new Promise((resolve, reject) => {
      fileData.findOne(
        { fileName: params.fileName },
        { fileData: { $elemMatch: { SrNo: params.SrNo } } },
        (err, response) => {
          if (err) {
            console.log(`err in findOne Title: ${util.inspect(err)}`);
            reject(err);
          } else {
            if (response.fileData) {
              fileData.updateOne(
                {
                  fileName: params.fileName,
                  "fileData.SrNo": params.SrNo,
                },
                {
                  $set: {
                    "fileData.$.EventID": params.EventID,
                    "fileData.$.Channel": params.Channel,
                    "fileData.$.DATE": params.DATE,
                    "fileData.$.TITLE": params.TITLE,
                    "fileData.$.DURATION": params.DURATION,
                  },
                },
                { multi: true },
                (err, fileData) => {
                  if (err) {
                    console.log(`err in findOne Title: ${util.inspect(err)}`);
                    reject(err);
                  } else {
                    resolve(fileData);
                  }
                }
              );
            }
          }
        }
      );
    });
  }

  // wellness Update in fileData
  findwellnessOneAndUpdate(params) {
    return new Promise((resolve, reject) => {
      fileData.findOne(
        { fileName: params.fileName },
        { fileData: { $elemMatch: { SrNo: params.SrNo } } },
        (err, response) => {
          if (err) {
            console.log(`err in findOne Title: ${util.inspect(err)}`);
            reject(err);
          } else {
            if (response.fileData) {
              fileData.updateOne(
                {
                  fileName: params.fileName,
                  "fileData.SrNo": params.SrNo,
                },
                {
                  $set: {
                    "fileData.$.ProgrammeName": params.ProgrammeName,
                    "fileData.$.TOPIC": params.TOPIC,
                    "fileData.$.Subject": params.Subject,
                    "fileData.$.CodeNo": params.CodeNo,
                    "fileData.$.Duration": params.Duration,
                  },
                },
                { multi: true },
                (err, fileData) => {
                  if (err) {
                    console.log(`err in findOne Title: ${util.inspect(err)}`);
                    reject(err);
                  } else {
                    resolve(fileData);
                  }
                }
              );
            }
          }
        }
      );
    });
  }

  // onetake Update in fileData
  findOnetakeOneAndUpdate(params) {
    return new Promise((resolve, reject) => {
      fileData.findOne(
        { fileName: params.fileName },
        { fileData: { $elemMatch: { SrNo: params.SrNo } } },
        (err, response) => {
          if (err) {
            console.log(`err in findOne Title: ${util.inspect(err)}`);
            reject(err);
          } else {
            if (response.fileData) {
              fileData.updateOne(
                {
                  fileName: params.fileName,
                  "fileData.SrNo": params.SrNo,
                },
                {
                  $set: {
                    "fileData.$.Name": params.Name,
                    "fileData.$.ContentID": params.ContentID,
                    "fileData.$.StartDate": params.StartDate,
                    "fileData.$.ProgramName": params.ProgramName,
                    "fileData.$.ProgramFile": params.ProgramFile,
                    "fileData.$.OnAirTime": params.OnAirTime,
                    "fileData.$.Duration": params.Duration,
                  },
                },
                { multi: true },
                (err, fileData) => {
                  if (err) {
                    console.log(`err in findOne Title: ${util.inspect(err)}`);
                    reject(err);
                  } else {
                    resolve(fileData);
                  }
                }
              );
            }
          }
        }
      );
    });
  }

  findOneAndUpdatePitaara(params) {
    return new Promise((resolve, reject) => {
      fileData.findOne(
        { fileName: params.fileName },
        { fileData: { $elemMatch: { N: params.SrNo } } },
        (err, response) => {
          if (err) {
            console.log(`err in findOne Title: ${util.inspect(err)}`);
            reject(err);
          } else {
            if (response.fileData) {
              fileData.updateOne(
                {
                  fileName: params.fileName,
                  "fileData.N": params.SrNo,
                },
                {
                  $set: {
                    // "fileData.$.StartDate": params.StartDate,
                    "fileData.$.Category": params.ContentID,
                    "fileData.$.TAPE ID": params.ProgramName,
                    "fileData.$.FPC_PROGRAMME_NAME": params.ProgramFile,
                    "fileData.$.Start time": params.OnAirTime,
                    "fileData.$.Duration": params.DURATION,
                  },
                },
                { multi: true },
                (err, fileData) => {
                  if (err) {
                    console.log(`err in findOne Title: ${util.inspect(err)}`);
                    reject(err);
                  } else {
                    resolve(fileData);
                  }
                }
              );
            }
          }
        }
      );
    });
  }

  findOneAndUpdateGreenChillies(params) {
    return new Promise((resolve, reject) => {
      fileData.findOne(
        { fileName: params.fileName },
        { fileData: { $elemMatch: { SrNo: params.SrNo } } },
        (err, response) => {
          if (err) {
            console.log(`err in findOne Title: ${util.inspect(err)}`);
            reject(err);
          } else {
            if (response.fileData) {
              fileData.updateOne(
                {
                  fileName: params.fileName,
                  "fileData.SrNo": params.SrNo,
                },
                {
                  $set: {
                    // "fileData.$.StartDate": params.StartDate,
                    "fileData.$.Start Time": params.StartTime,
                    "fileData.$.End Time": params.EndTime,
                    "fileData.$.Clip ID": params.ClipId,
                    "fileData.$.Event": params.Event,
                    "fileData.$.Duration": params.Duration,
                  },
                },
                { multi: true },
                (err, fileData) => {
                  if (err) {
                    console.log(`err in findOne Title: ${util.inspect(err)}`);
                    reject(err);
                  } else {
                    resolve(fileData);
                  }
                }
              );
            }
          }
        }
      );
    });
  }
  //   Fancode query
  findOneAndUpdateFancode(params) {
    return new Promise((resolve, reject) => {
      fileData.findOne(
        { fileName: params.fileName },
        { fileData: { $elemMatch: { SrNo: params.SrNo } } },
        (err, response) => {
          if (err) {
            console.log(`err in findOne Title: ${util.inspect(err)}`);
            reject(err);
          } else {
            if (response.fileData) {
              fileData.updateOne(
                {
                  fileName: params.fileName,
                  "fileData.SrNo": params.SrNo,
                },
                {
                  $set: {
                    // "fileData.$.StartDate": params.StartDate,
                    "fileData.$.Category": params.ContentID,
                    "fileData.$.TAPE ID": params.ProgramName,
                    "fileData.$.FPC_PROGRAMME_NAME": params.ProgramFile,
                    "fileData.$.Start time": params.OnAirTime,
                    "fileData.$.Duration": params.Duration,
                  },
                },
                { multi: true },
                (err, fileData) => {
                  if (err) {
                    console.log(`err in findOne Title: ${util.inspect(err)}`);
                    reject(err);
                  } else {
                    resolve(fileData);
                  }
                }
              );
            }
          }
        }
      );
    });
  }
}

module.exports = {
  fileDataClass: fileDataCls,
};
