var query = function (field, url) {
  var href = url ? url : window.location.href;
  var reg = new RegExp("[?&]" + field + "=([^&#]*)", "i");
  var string = reg.exec(href);
  return string ? string[1] : null;
};
var fileName = query("fileName") ? query("fileName") : null;

var filename = "";
$.get(
  `/v1/fileManagement/getFileDatabyFilename?fileName=${fileName}`,
  function (data, status) {
    console.log("abcd data", data);
    if (data.status === 200) {
      filename = data.data.fileName;
      var ClientName = data.data.UserName;
      var GetfileData = data.data.fileData;

      if (ClientName === "onetake") {
        appendOneTakeFiledatabyfileName(GetfileData);
      } else if (ClientName === "wellness") {
        appendFiledatabyfileName(GetfileData);
      } else if (ClientName === "srmd") {
        var GetSrmdfileData = data.data.fileData;
        appendFiledatabyfileNamewithSrmd(GetSrmdfileData);
      } else if (ClientName === "harekrishna") {
        var GetHarekrishnafileData = data.data.fileData;
        appendFiledatabyfileNamewithHarekrishna(GetHarekrishnafileData);
      } else if (ClientName === "pitaara") {
        var GetPitaarafileData = data.data.fileData;
        appendFiledatabyfileNamewithPitaara(GetPitaarafileData);
      } else if (ClientName === "greenchillies") {
        var GetPitaarafileData = data.data.fileData;
        appendFiledatabyfileNamewithGreenChillies(GetPitaarafileData);
      } else if (ClientName === "fancode") {
        var GetFancodeFileData = data.data.fileData;
        appendFiledatabyfileNamewithFancode(GetFancodeFileData);
      }
    } else {
      console.log("get getFileDatabyFilename Failed");
    }
  }
);

function appendFiledatabyfileName(GetfileData) {
  var array = GetfileData;
  var options_table = "";

  array.forEach(function (element, i) {
    var SrNo = element.SrNo ? element.SrNo : "-";
    var ProgrammeName = element.ProgrammeName ? element.ProgrammeName : "-";
    var TOPIC = element.TOPIC ? element.TOPIC : "-";
    var Subject = element.Subject ? element.Subject : "-";
    var CodeNo = element.CodeNo ? element.CodeNo : "-";
    var Duration = element.Duration ? element.Duration : "-";
    options_table += `<tr class="users-tbl-row asset-row" dataId=${i}>
             <td class="${i + 1} SrNo" dataSrNo="${SrNo}">${SrNo}</td>
             <td class="name ProgrammeName" id="ProgrammeName">${ProgrammeName}</td>
             <td class="name TOPIC" id="TOPIC">${TOPIC}</td>
             <td class="name Subject" id="Subject">${Subject}</td>
             <td class="name CodeNo" id="CodeNo">${CodeNo}</td>
             <td class="name Duration" id="Duration">${Duration}</td>`;
    if (i == array.length - 1) {
      //initiate for 1st row
      $("#xldata_tbody").append(options_table);
      $("#xldata_table").Tabledit({
        buttons: {
          edit: {
            class: "btn btn-sm btn-info edit-xls-btn",
            html: `<span class="mdi mdi-pencil"></span>`,
            action: "edit",
          },
          save: {
            class: "btn btn-sm btn-success save-xls-btn-wellness",
            html: `<span class="mdi mdi-check"></span>`,
          },
        },

        inputClass: "form-control form-control-sm",
        deleteButton: !1,
        autoFocus: !1,
        columns: {
          identifier: [0, "id"],
          editable: [
            [1, "col1"],
            [2, "col2"],
            [3, "col3"],
            [4, "col4"],
            [5, "col5"],
          ],
        },
      });
      $("#xldata_table").DataTable({
        dom: "Bfrtip",
        buttons: ["excelHtml5"],
      });
    }
  });
  $("#sample_div").addClass("display-hidden");
  $("#sample_divOntake").addClass("display-hidden");
  $("#harekrishna_div").addClass("display-hidden");
  $("#pitaara_main_div").addClass("display-hidden");
  $("#greenchillies_main_div").addClass("display-hidden");
}

$(document).on("click", ".save-xls-btn-wellness", function (event) {
  const params = {
    ProgrammeName: $(this).closest("tr").find("#ProgrammeName").text(),
    SrNo: $(this).closest("tr").find("td.SrNo").attr("dataSrNo"),
    TOPIC: $(this).closest("tr").find("#TOPIC").text(),
    Subject: $(this).closest("tr").find("#Subject").text(),
    CodeNo: $(this).closest("tr").find("#CodeNo").text(),
    Duration: $(this).closest("tr").find("#Duration").text(),
    fileName: filename,
  };

  $.post(
    "/v1/fileManagement/wellnessDataUpdate",
    params,
    function (data, status) {
      if (data.status === 200) {
      }
    }
  );
});

function appendFiledatabyfileNamewithSrmd(GetSrmdfileData) {
  var array = GetSrmdfileData;
  var options_table = "";

  array.forEach(function (element, i) {
    var DATE = element.DATE ? element.DATE : "-";
    var Event_ID = element.EventID ? element.EventID : "-";
    var TITLE = element.TITLE ? element.TITLE : "-";
    var DURATION = element.DURATION ? element.DURATION : "-";
    var TXTIME = element.TXTIME ? element.TXTIME : "-";
    var SrNo = element.SrNo ? element.SrNo : "-";

    options_table += `<tr class="users-tbl-row asset-row" dataId=${i}>
        <td class="SrNo" data-SrNo="${SrNo}">${SrNo}</td>

   <td class="name" style="width:10%" id="date">${DATE}</td>
   <td class="name eventId" id="eventId">${Event_ID}</td>
   <td class="name title" id="title">${TITLE}</td>
   <td class="name TXTIME" id="TXTIME">${TXTIME}</td>
   <td class="name duration" id="duration">${DURATION}</td>`;

    if (i == array.length - 1) {
      //initiate for 1st row
      $("#srmdxldata_tbody").append(options_table);
      $("#srmdxldata_table").Tabledit({
        buttons: {
          edit: {
            class: "btn btn-sm btn-info edit-xls-btn",
            html: `<span class="mdi mdi-pencil"></span>`,
            action: "edit",
          },
          save: {
            class: "btn btn-sm btn-success save-xls-btn",
            html: `<span class="mdi mdi-check"></span>`,
          },
        },
        inputClass: "form-control form-control-sm",
        deleteButton: !1,
        autoFocus: !1,
        columns: {
          identifier: [0, "id"],
          editable: [
            [1, "col1"],
            [2, "col2"],
            [3, "col3"],
            [4, "col4"],
            [5, "col5"],
          ],
        },
      });
      $("#srmdxldata_table").DataTable({
        dom: "Bfrtip",
        buttons: ["excelHtml5"],
      });
    }
  });

  $("#sample_div1").addClass("display-hidden");
  $("#sample_divOntake").addClass("display-hidden");
  $("#harekrishna_div").addClass("display-hidden");
  $("#pitaara_main_div").addClass("display-hidden");
  $("#greenchillies_main_div").addClass("display-hidden");
}

$(document).on("click", ".save-xls-btn", function (event) {
  const params = {
    SrNo: $(this).closest("tr").find("td.SrNo").attr("data-SrNo"),
    TITLE: $(this).closest("tr").find("#title").text(),
    EventID: $(this).closest("tr").find("#eventId").text(),
    Channel: $(this).closest("tr").find("#channel").text(),
    DATE: $(this).closest("tr").find("#date").text(),
    DURATION: $(this).closest("tr").find("#duration").text(),
    fileName: filename,
  };
  $.post("/v1/fileManagement/srmdDataUpdate", params, function (data, status) {
    if (data.status === 200) {
    }
  });
});

function appendOneTakeFiledatabyfileName(GetfileData) {
  var array = GetfileData;
  var options_table = "";
  array.forEach(function (element, i) {
    var Name = element.Name ? element.Name : "-";
    var SimpleID = element.SimpleID ? element.SimpleID : "-";
    var StartDate = element.StartDate;
    var ContentID = element.ContentID ? element.ContentID : "-";
    var ProgramName = element.ProgramName ? element.ProgramName : "-";
    var ProgramFile = element.ProgramFile ? element.ProgramFile : "-";
    var InPoint = element.InPoint ? element.InPoint : "-";
    var OutPoint = element.OutPoint ? element.OutPoint : "-";
    var OnAirTime = element.OnAirTime ? element.OnAirTime : "-";
    var Duration = element.Duration ? element.Duration : "-";
    var SrNo = element.SrNo ? element.SrNo : "-";

    options_table += `<tr class="users-tbl-row asset-row" dataId=${i}>
                  <td class="SrNo" data-SrNo="${SrNo}">${SrNo}</td>
                  <td class="name StartDate" id="StartDate">${StartDate}</td>
                  <td class="name ContentID" id="ContentID">${ContentID}</td>
                  <td class="name ProgramName" id="ProgramName">${ProgramName}</td>
                  <td class="name ProgramFile" id="ProgramFile">${ProgramFile}</td>
                  <td class="name OnAirTime" id="OnAirTime">${OnAirTime}</td>
                  <td class="name Duration" id="Duration">${Duration}</td>`;

    if (i == array.length - 1) {
      //initiate for 1st row
      $("#xlOnetakedata_tbody").append(options_table);
      $("#xlOnetakedata_table").Tabledit({
        buttons: {
          edit: {
            class: "btn btn-sm btn-info edit-xls-btn",
            html: `<span class="mdi mdi-pencil"></span>`,
            action: "edit",
          },
          save: {
            class: "btn btn-sm btn-success save-xls-btn-onetake",
            html: `<span class="mdi mdi-check"></span>`,
          },
        },
        inputClass: "form-control form-control-sm",
        deleteButton: !1,
        autoFocus: !1,
        columns: {
          identifier: [0, "id"],
          editable: [
            [1, "col1"],
            [2, "col2"],
            [3, "col3"],
            [4, "col4"],
            [5, "col5"],
            [6, "col6"],
          ],
        },
      });
      $("#xlOnetakedata_table").DataTable({
        dom: "Bfrtip",
        buttons: ["excelHtml5"],
      });
    }
  });
  $("#sample_div").addClass("display-hidden");
  $("#sample_div1").addClass("display-hidden");
  $("#harekrishna_div").addClass("display-hidden");
  $("#pitaara_main_div").addClass("display-hidden");
  $("#greenchillies_main_div").addClass("display-hidden");
}

$(document).on("click", ".save-xls-btn-onetake", function (event) {
  const params = {
    Name: $(this).closest("tr").find("#Name").text(),
    SrNo: $(this).closest("tr").find("td.SrNo").attr("data-SrNo"),
    ContentID: $(this).closest("tr").find("#ContentID").text(),
    StartDate: $(this).closest("tr").find("#StartDate").text(),
    ProgramName: $(this).closest("tr").find("#ProgramName").text(),
    ProgramFile: $(this).closest("tr").find("#ProgramFile").text(),
    OnAirTime: $(this).closest("tr").find("#OnAirTime").text(),
    Duration: $(this).closest("tr").find("#Duration").text(),
    fileName: filename,
  };
  $.post(
    "/v1/fileManagement/OnetakeDataUpdate",
    params,
    function (data, status) {
      if (data.status === 200) {
      }
    }
  );
});

$(document).on("click", ".save-xls-btn-pitaara", function (event) {
  const params = {
    Name: $(this).closest("tr").find("#Name").text(),
    SrNo: $(this).closest("tr").find("td.SrNo").attr("data-SrNo"),
    ContentID: $(this).closest("tr").find("#ContentID").text(),
    StartDate: $(this).closest("tr").find("#StartDate").text(),
    ProgramName: $(this).closest("tr").find("#ProgramName").text(),
    ProgramFile: $(this).closest("tr").find("#ProgramFile").text(),
    OnAirTime: $(this).closest("tr").find("#OnAirTime").text(),
    Duration: $(this).closest("tr").find("#Duration").text(),
    fileName: filename,
  };
  console.log("PITAARA single row update");

  $.post("/v1/fileManagement/PitaaraUpdate", params, function (data, status) {
    if (data.status === 200) {
      toastr.success(data.message);
    }
  });
});

$(document).on("click", ".save-xls-btn-greenchillies", function (event) {
  const params = {
    SrNo: $(this).closest("tr").find("td.SrNo").attr("data-SrNo"),
    StartTime: $(this).closest("tr").find("#StartTime").text(),
    EndTime: $(this).closest("tr").find("#EndTime").text(),
    ClipId: $(this).closest("tr").find("#ClipId").text(),
    Event: $(this).closest("tr").find("#Event").text(),
    Duration: $(this).closest("tr").find("#Duration").text(),
    fileName: filename,
  };
  // console.log("GREEn CHILLIES single row update",params)

  $.post(
    "/v1/fileManagement/GreenChilliesUpdate",
    params,
    function (data, status) {
      console.log("Main update");
      if (data.status === 200) {
        toastr.success(data.message);
      }
    }
  );
});

$(document).on("click", ".save-xls-btn-fancode", function (event) {
  const params = {
    Name: $(this).closest("tr").find("#Name").text(),
    SrNo: $(this).closest("tr").find("td.SrNo").attr("data-SrNo"),
    ContentID: $(this).closest("tr").find("#ContentID").text(),
    StartDate: $(this).closest("tr").find("#StartDate").text(),
    ProgramName: $(this).closest("tr").find("#ProgramName").text(),
    ProgramFile: $(this).closest("tr").find("#ProgramFile").text(),
    OnAirTime: $(this).closest("tr").find("#OnAirTime").text(),
    Duration: $(this).closest("tr").find("#Duration").text(),
    fileName: filename,
  };
  console.log("Fancode  row update");

  $.post("/v1/fileManagement/FancodeUpdate", params, function (data, status) {
    if (data.status === 200) {
      toastr.success(dsingleata.message);
    }
  });
});

function appendFiledatabyfileNamewithHarekrishna(GetHarekrishnafileData) {
  var array = GetHarekrishnafileData;
  var options_table = "";

  array.forEach(function (element, i) {
    var AiringStartTime = element.AiringStartTime
      ? element.AiringStartTime
      : "-";
    var EndTime = element.EndTime ? element.EndTime : "-";
    var ProgramName = element.ProgramName ? element.ProgramName : "-";
    var FileName = element.FileName ? element.FileName : "-";
    var FillersSerialNumber = element.FillersSerialNumber
      ? element.FillersSerialNumber
      : "-";
    var EpisodeTitle = element.EpisodeTitle ? element.EpisodeTitle : "-";
    var EpisodeNumber = element.EpisodeNumber ? element.EpisodeNumber : "-";
    var StarCast = element.StarCast ? element.StarCast : "-";
    var POPUPImageVideo_1 = element["POPUPImage/Video_1"]
      ? element["POPUPImage/Video_1"]
      : "";
    var POPUPImageVideo_2 = element["POPUPImage/Video_2"]
      ? element["POPUPImage/Video_2"]
      : "";
    var POPUPImageVideo_3 = element["POPUPImage/Video_3"]
      ? element["POPUPImage/Video_3"]
      : "";

    options_table += `<tr class="users-tbl-row asset-row" dataId=${i}>
     <td class="index">${i + 1}</td>
     <td class="name" style="width:10%" id="AiringStartTime">${AiringStartTime}</td>
     <td class="name EndTime">${EndTime}</td>
     <td class="name ProgramName" id="ProgramName">${ProgramName}</td>
     <td class="name FileName" id="FileName">${FileName}</td>
     <td class="name FillersSerialNumber" id="FillersSerialNumber">${FillersSerialNumber}</td>
     <td class="name EpisodeTitle" id="EpisodeTitle">${EpisodeTitle}</td>
     <td class="name StarCast" id="StarCast">${StarCast}</td>
     <td class="name POPUPImageVideo_1" id="POPUPImageVideo_1">${POPUPImageVideo_1}</td>
     <td class="name POPUPImageVideo_2" id="POPUPImageVideo_2">${POPUPImageVideo_2}</td>
     <td class="name POPUPImageVideo_3" id="POPUPImageVideo_3">${POPUPImageVideo_3}</td>
     <td class="name EpisodeNumber" id="EpisodeNumber">${EpisodeNumber}</td>`;

    if (i == array.length - 1) {
      //initiate for 1st row
      $("#harekrishnaxldata_tbody").append(options_table);
      $("#harekrishnaxldata_table").DataTable({
        dom: "Bfrtip",
        buttons: ["excelHtml5"],
      });
    }
  });

  $("#sample_div1").addClass("display-hidden");
  $("#sample_divOntake").addClass("display-hidden");
  $("#sample_div").addClass("display-hidden");
  $("#pitaara_main_div").addClass("display-hidden");
  $("#greenchillies_main_div").addClass("display-hidden");
}

function appendFiledatabyfileNamewithPitaara(GetPitaarafileData) {
  var array = GetPitaarafileData;
  var options_table = "";
  console.log("GET FILES OF PITAARA");

  array.forEach(function (element, i) {
    // console.log("GET FILES OF PITAARA",element)
    var StartDate = element["Start date"] ? element["Start date"] : "-";
    var ContentID = element.Category ? element.Category : "-";
    var ProgramName = element["TAPE ID"] ? element["TAPE ID"] : "-";
    var ProgramFile = element["FPC_PROGRAMME_NAME"]
      ? element["FPC_PROGRAMME_NAME"]
      : "-";
    var OnAirTime = element["Start time"] ? element["Start time"] : "-";
    var Duration = element.Duration ? element.Duration : "-";
    var SrNo = element["N"] ? element["N"] : "-";

    options_table += `<tr class="users-tbl-row asset-row" dataId=${i}>
                  <td class="SrNo" data-SrNo="${SrNo}">${SrNo}</td>
                  <td class="name StartDate" id="StartDate">${StartDate}</td>
                  <td class="name ContentID" id="ContentID">${ContentID}</td>
                  <td class="name ProgramName" id="ProgramName">${ProgramName}</td>
                  <td class="name ProgramFile" id="ProgramFile">${ProgramFile}</td>
                  <td class="name OnAirTime" id="OnAirTime">${OnAirTime}</td>
                  <td class="name Duration" id="Duration">${Duration}</td>`;

    if (i == array.length - 1) {
      //initiate for 1st row
      $("#pitaara_data_tbody").append(options_table);
      $("#pitaara_data_table").Tabledit({
        buttons: {
          edit: {
            class: "btn btn-sm btn-info edit-xls-btn",
            html: `<span class="mdi mdi-pencil"></span>`,
            action: "edit",
          },
          save: {
            class: "btn btn-sm btn-success save-xls-btn-pitaara",
            html: `<span class="mdi mdi-check"></span>`,
          },
        },
        inputClass: "form-control form-control-sm",
        deleteButton: !1,
        autoFocus: !1,
        columns: {
          identifier: [0, "id"],
          editable: [
            [1, "col1"],
            [2, "col2"],
            [3, "col3"],
            [4, "col4"],
            [5, "col5"],
            [6, "col6"],
          ],
        },
      });
      $("#xlOnetakedata_table").DataTable({
        dom: "Bfrtip",
        buttons: ["excelHtml5"],
      });
    }
  });

  $("#sample_div1").addClass("display-hidden");
  $("#sample_divOntake").addClass("display-hidden");
  $("#sample_div").addClass("display-hidden");
  $("#harekrishna_div").addClass("display-hidden");
  $("#greenchillies_main_div").addClass("display-hidden");
}

function appendFiledatabyfileNamewithFancode(GetFancodeFileData) {
  var array = GetFancodeFileData;
  var options_table = "";
  console.log("GET FILES OF FANCODE");

  array.forEach(function (element, i) {
    var StartDate = element["Start date"] ? element["Start date"] : "-";
    var ContentID = element.Category ? element.Category : "-";
    var ProgramName = element["TAPE ID"] ? element["TAPE ID"] : "-";
    var ProgramFile = element["FPC_PROGRAMME_NAME"]
      ? element["FPC_PROGRAMME_NAME"]
      : "-";
    var OnAirTime = element["Start time"] ? element["Start time"] : "-";
    var Duration = element.Duration ? element.Duration : "-";
    var SrNo = element["N"] ? element["N"] : "-";

    options_table += `<tr class="user-tbl-row asset-row" dataId=${i}>
    <td class="SrNo" data-SrNo="${SrNo}">${SrNo}</td>
    <td calss = "name StartDate" id="StartDate">${StartDate}</td>
    <td class = "name ContentID" id="ContentID">${ContentID}</td>
    <td class ="name ProgramName" id="ProgramName">${ProgramName}</td>
    <td class="name ProgramFile" id="ProgramFile">${ProgramFile}</td>
    <td class = "name OnAirTime" id="OnAirtime">${OnAirTime}</td>
    <td class = "name Duration" id="Duration">${Duration}</td>
    `;

    if (i == array.length - 1) {
      $("#fancode_data_tbody").append(options_table);
      $("#fancode_data_table").Tabledit({
        buttons: {
          edit: {
            class: "btn btn-sm btn-info edit-xls-btn",
            html: `<span class="mdi mdi-pencil></span>`,
            action: "edit",
          },
          save: {
            class: "btn btn-sm btn-success save-xls-btn-fancode",
            html: `<span class="mdi mdi-check"></span>`,
          },
        },
        inputClass: "form-control form-control-sm",
        deleteButton: !1,
        autoFocus: !1,
        columns: {
          identifier: [0, "id"],
          editable: [
            [1, "col1"],
            [2, "col2"],
            [3, "col3"],
            [4, "col4"],
            [5, "col5"],
            [6, "col6"],
          ],
        },
      });
      $("#xlOnetakedata_table").DataTable({
        dom: "Bfrtip",
        buttons: ["excelHtml5"],
      });
    }
  });
  $("#sample_div1").addClass("display-hidden");
  $("#sample_divOntake").addClass("display-hidden");
  $("#sample_div").addClass("display-hidden");
  $("#harekrishna_div").addClass("display-hidden");
  $("#greenchillies_main_div").addClass("display-hidden");
}

function appendFiledatabyfileNamewithGreenChillies(GetPitaarafileData) {
  var array = GetPitaarafileData;
  var options_table = "";
  console.log("GET FILES OF GREEN CHILLIES");

  array.forEach(function (element, i) {
    // console.log("GET FILES OF PITAARA",element)
    var StartTime = element["Start Time"] ? element["Start Time"] : "-";
    var EndTime = element["End Time"] ? element["End Time"] : "-";
    var ClipId = element["Clip ID"] ? element["Clip ID"] : "-";
    var Event = element.Event?.trim() ? element.Event.trim() : "-";
    var Duration = element.Duration ? element.Duration : "-";
    var SrNo = element["SrNo"] ? element["SrNo"] : "-";

    options_table += `<tr class="users-tbl-row asset-row" dataId=${i}>
                  <td class="SrNo" data-SrNo="${SrNo}">${SrNo}</td>
                  <td class="name StartDate" id="StartTime">${StartTime}</td>
                  <td class="name ContentID" id="EndTime">${EndTime}</td>
                  <td class="name ProgramName" id="ClipId">${ClipId}</td>
                  <td class="name ProgramFile" id="Event">${Event}</td>
                  <td class="name Duration" id="Duration">${Duration}</td>`;

    if (i == array.length - 1) {
      //initiate for 1st row
      $("#greenchillies_data_tbody").append(options_table);
      $("#greenchillies_data_table").Tabledit({
        buttons: {
          edit: {
            class: "btn btn-sm btn-info edit-xls-btn",
            html: `<span class="mdi mdi-pencil"></span>`,
            action: "edit",
          },
          save: {
            class: "btn btn-sm btn-success save-xls-btn-greenchillies",
            html: `<span class="mdi mdi-check"></span>`,
          },
        },
        inputClass: "form-control form-control-sm",
        deleteButton: !1,
        autoFocus: !1,
        columns: {
          identifier: [0, "id"],
          editable: [
            [1, "col1"],
            [2, "col2"],
            [3, "col3"],
            [4, "col4"],
            [5, "col5"],
            [6, "col6"],
          ],
        },
      });
      $("#xlOnetakedata_table").DataTable({
        dom: "Bfrtip",
        buttons: ["excelHtml5"],
      });
    }
  });

  $("#sample_div1").addClass("display-hidden");
  $("#sample_divOntake").addClass("display-hidden");
  $("#sample_div").addClass("display-hidden");
  $("#harekrishna_div").addClass("display-hidden");
  $("#pitaara_main_div").addClass("display-hidden");
}

$(document).ready(function () {
  $.get("/getUsers", function (data, status) {
    console.log("getUsers", data);
    if (data.status == 200) {
      appendUsersDatatable(data);
    }
  });

  function appendUsersDatatable(data) {
    var array = data.data.Users;
    if (array.length) {
      var options_table = "";
      var string2 = "";
      array.forEach(function (element, i) {
        var Username = element.Username ? element.Username : "";
        var Attributes = element.Attributes ? element.Attributes : "";
        var UserCreateDate = element.UserCreateDate
          ? moment(element.UserCreateDate).format("lll")
          : "";
        var UserLastModifiedDate = element.UserLastModifiedDate
          ? moment(element.UserLastModifiedDate).format("lll")
          : "";
        var Enabled = element.Enabled ? element.Enabled : false;
        var UserStatus = element.UserStatus
          ? element.UserStatus
          : "UNCONFIRMED";
        var name = "";
        var string = "";

        if (UserStatus === "CONFIRMED") {
          UserStatus = '<span class="badge badge-success">Active</span>';
        } else {
          UserStatus =
            '<span class="badge badge-purple">Pending Activation</span>';
        }

        if (Attributes.length) {
          for (var j = 0; j < Attributes.length; j++) {
            if (Attributes[j].Name === "name") {
              name = Attributes[j].Value;
            }
          }
        }

        string += `<option class="user-name pro-user-role" value='${name}' userName='${Username}' >${name}</option>`;

        string2 += `<option class="user-name" id="getuserfiledata" value='${Username}' userName='${Username}' >${Username}</option>`;

        if (i == array.length - 1) {
          $("#userName").append(string);
          $("#Customer_Name").append(string2);
        }
      });
    }
  }

  var ClientName = "";
  $("#getfiledata").submit(function (event) {
    let params = {
      Username: $("option:selected", this).attr("Username"),
    };
    ClientName = params.Username;

    // getfileJSON
    $.post(
      `/v1/fileManagement/getUserFileData`,
      params,
      function (data, status) {
        if (data.status == 200) {
          var converteddata = JSON.stringify(data.data);
          destroyRowsxllist();
          functionFileData(converteddata);
          $("#tableDiv").removeClass("display-hidden");
        }
      }
    );
    event.preventDefault();
  });

  function destroyRowsxllist() {
    $("#xllist_tbody").empty();
    $("#xllist_table").DataTable().rows().remove();
    $("#xllist_table").DataTable().destroy();
  }

  function checkIfStringContainsAphabet(name) {
    const regExp = /[a-zA-Z]/g;
    if (regExp.test(name)) return true;
    return false;
  }

  function removeSheetName(fileName) {
    const splitByDot = fileName.split(".");
    const splitByUnderscore = splitByDot[splitByDot.length - 2].split("_");
    const lastChunk = splitByUnderscore[splitByUnderscore.length - 1];
    if (checkIfStringContainsAphabet(lastChunk)) {
      splitByUnderscore.splice(splitByUnderscore.length - 1, 1);
      return splitByUnderscore.join("_") + "." + splitByDot.at(-1);
    }
    return fileName;
  }

  function functionFileData(converteddata) {
    var array = converteddata;
    var abc = JSON.parse(array);
    var Client = ClientName;
    var options_table = "";
    abc.forEach(function (element, i) {
      var fileName = element.fileName ? element.fileName : "";
      var UserName = element.UserName ? element.UserName : "";
      var ChannelName = element.ChannelName ? element.ChannelName : "-";
      var created_at = element.created_at
        ? moment(element.created_at).format("lll")
        : "";
      var updated_at = element.updated_at
        ? moment(element.updated_at).format("lll")
        : "";

      if (Client === "harekrishna") {
        options_table += `<tr class="users-tbl-row asset-row">

            <td class="username listObjects" key_factor="${fileName}"><a class="group-name-link"  href="/v1/users/user-xlsheetdata?fileName=${fileName}">${fileName}</a></td>
            <td class="name">${ChannelName}</td>
            <td class="name">${UserName}</td>
            <td class="name">${created_at}</td>

           <td class="action-td" id=${fileName}><div class="dropdown">
           <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false"> <i class="fe-settings noti-icon"></i> </a> <div class="dropdown-menu dropdown-menu-right">
           <a href="#" userName="${UserName}" fileName="${fileName}" class="dropdown-item download-Airfile">Download Air File</a>
            <a href="#" userName="${UserName}" fileName="${fileName}" class="dropdown-item download-epgfile">Download EPG File</a>
            <a href="#" userName="${UserName}" fileName="${fileName}" class="dropdown-item download-nowFile">Download Now File</a>
            <a href="#" userName="${UserName}" fileName="${fileName}" class="dropdown-item download-popFile">Download POP Up File</a>
           </div> </div></td>`;
      } else if (Client === "pitaara") {
        options_table += `<tr class="users-tbl-row asset-row">

                    <td class="username listObjects" key_factor="${fileName}"><a class="group-name-link"  href="/v1/users/user-xlsheetdata?fileName=${fileName}">${fileName}</a></td>
                    <td class="name">${ChannelName}</td>
                    <td class="name">${UserName}</td>
                    <td class="name">${created_at}</td>

                    <td class="action-td" id=${fileName}><div class="dropdown"> <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false"> <i class="fe-settings noti-icon"></i> </a> <div class="dropdown-menu dropdown-menu-right">
                    <a href="#" userName="${UserName}" fileName="${fileName}" class="dropdown-item download-file-2">Download Air File</a>
                    <a href="#" userName="${UserName}" fileName="${fileName}" class="dropdown-item download-Playlist">Download Playlist File</a>
                    <a href="#" userName="${UserName}" fileName="${fileName}" class="dropdown-item download-nowFile">Download Now File</a>


                    </div> </div></td>`;
      } else if (Client === "greenchillies") {
        console.log("Inside green Chillies");

        options_table += `<tr class="users-tbl-row asset-row">

                    <td class="username listObjects" key_factor="${fileName}"><a class="group-name-link"  href="/v1/users/user-xlsheetdata?fileName=${fileName}">${fileName}</a></td>
                    <td class="name">${ChannelName}</td>
                    <td class="name">${UserName}</td>
                    <td class="name">${created_at}</td>

                    <td class="action-td" id=${fileName}><div class="dropdown"> <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false"> <i class="fe-settings noti-icon"></i> </a> <div class="dropdown-menu dropdown-menu-right">
                    <a href="#" userName="${UserName}" fileName="${fileName}" class="dropdown-item download-file-2">Download Air File</a>
                    <a href="#" userName="${UserName}" fileName="${removeSheetName(
          fileName
        )}" class="dropdown-item download-Playlist">Download Playlist File</a>

                    </div> </div></td>`;
      } else {
        options_table += `<tr class="users-tbl-row asset-row">

            <td class="username listObjects" key_factor="${fileName}"><a class="group-name-link"  href="/v1/users/user-xlsheetdata?fileName=${fileName}">${fileName}</a></td>
            <td class="name">${ChannelName}</td>
            <td class="name">${UserName}</td>
            <td class="name">${created_at}</td>

           <td class="action-td" id=${fileName}><div class="dropdown"> <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false"> <i class="fe-settings noti-icon"></i> </a> <div class="dropdown-menu dropdown-menu-right">
           <a href="#" userName="${UserName}" fileName="${fileName}" class="dropdown-item download-file-2">Download Air File</a>
           <a href="#" userName="${UserName}" fileName="${fileName}" class="dropdown-item download-Playlist">Download Playlist File</a>
           <a href="#" userName="${UserName}" fileName="${fileName}" class="dropdown-item download-epgfile">Download EPG File</a>
           <a href="#" userName="${UserName}" fileName="${fileName}" class="dropdown-item download-nowFile">Download Now File</a>


           <a href="/v1/fileManagement/UserEpgData?fileName=${fileName}" key-value="${fileName}" class="dropdown-item">View EPG File</a>
           </div> </div></td>`;
      }

      if (i == abc.length - 1) {
        $("#xllist_tbody").append(options_table);
        reInitialiClientSheetListTable();
      }
    });
  }

  function reInitialiClientSheetListTable() {
    $("#xllist_table").DataTable().destroy();
    xllist_table = $("#xllist_table").DataTable({
      //"order": [[1, "desc"]], // for descending order
      columnDefs: [
        { width: "3%", targets: 0 },
        { width: "3%", targets: 1 },
        { width: "3%", targets: 2 },
        { width: "3%", targets: 3 },
        { width: "3%", targets: 4 },
      ],
    });

    $("#xllist_table tbody tr:first").addClass("active");
  }
});

var userChannelName = "";
var file = "";
$(document).on("click", ".download-file-2", function (event) {
  var userName = $(this).attr("userName");
  var File_name = $(this).attr("fileName");
  let params = {
    Username: userName,
    fileName: File_name,
  };

  originalString = File_name;
  newSting2 = originalString.replace(".xlsx", "");
  file = newSting2 + ".air";

  $.post(
    `/v1/fileManagement/getFileDatabyFilenameForOutput`,
    params,
    function (data, status) {
      if (data.status == 200) {
        var jsonData = data.data.fileData;
        userChannelName = data.data.ChannelName;

        if (data.data.UserName === "wellness") {
          functionWellnessFileDownloadData(jsonData);
        } else if (data.data.UserName === "onetake") {
          functionOnetakeFileDownloadData(jsonData);
        } else if (data.data.UserName === "srmd") {
          functionFileDownloadDataSrmd(jsonData);
        } else if (data.data.UserName === "harekrishna") {
          fillers(jsonData);
        } else if (data.data.UserName === "pitaara") {
          console.log("Inside pitaara");
          functionPitaaraFileDownloadDataAir(jsonData);
        } else if (data.data.UserName === "greenchillies") {
          console.log("Inside greenchillies download");
          functionFileDownloadDataGreenChillies(jsonData);
        } else if (data.data.UserName === "greenchillies") {
          console.log("Inside greenchillies download");
          functionFileDownloadDataGreenChillies(jsonData);
        } else if (data.data.UserName === "fancode") {
          console.log("Inside fancode download");
          functionFancodeFileDownloadData(jsonData);
        }
      }
    }
  );
});

var hareuserChannelName = "";
var harefile = "";
$(document).on("click", ".download-Airfile", function (event) {
  var userName = $(this).attr("userName");
  var File_name = $(this).attr("fileName");

  let params = {
    Username: userName,
    fileName: File_name,
  };

  originalString = File_name;
  newSting2 = originalString.replace(".xlsx", "");
  harefile = newSting2 + ".air";

  $.post(
    `/v1/fileManagement/getFileDatabyFilenameForOutput`,
    params,
    function (data, status) {
      if (data.status == 200) {
        var jsonData = data.data.fileData;
        hareuserChannelName = data.data.ChannelName;

        if (data.data.UserName === "harekrishna") {
          fillers(jsonData);
        }
      }
    }
  );
});

$(document).on("click", ".download-Playlist", function (event) {
  var userName = $(this).attr("userName");
  var File_name = $(this).attr("fileName");

  window.location.href = `https://dev-skandha-vod.s3.ap-south-1.amazonaws.com/input-data/${File_name}`;
  toastr.success("Play List File Successfully downloaded!!");
});

$(document).on("click", ".download-epgfile", function (event) {
  var userName = $(this).attr("userName");
  var File_name = $(this).attr("fileName");

  let params = {
    Username: userName,
    fileName: File_name,
  };

  $.post(
    `/v1/fileManagement/getEPGFileDatabyFilename`,
    params,
    function (data, status) {
      console.log("epg file data", data);
      if (data.status == 200) {
        if (data.data === null) {
          Swal.fire({
            title: "No EPG file Available for this Channel",
            showClass: {
              popup: "animate__animated animate__fadeInDown",
            },
            hideClass: {
              popup: "animate__animated animate__fadeOutUp",
            },
          });
        } else {
          var epgFileName = data.data.fileName;
          window.location.href = `https://dev-skandha-vod.s3.ap-south-1.amazonaws.com/input-epg/${epgFileName}`;
          toastr.success("EPG File Successfully downloaded!!");
        }
      }
    }
  );
});

function download(file, text) {
  var element = document.createElement("a");
  element.setAttribute(
    "href",
    "data:text/plain;charset=utf-8,%EF%BB%BF" + encodeURIComponent(text)
  );
  element.setAttribute("download", file);
  element.style.display = "none";
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
}

function downloadNow(file, text) {
  var element = document.createElement("a");
  element.setAttribute(
    "href",
    "data:text/plain;charset=utf-8,%EF%BB%BF" + encodeURIComponent(text)
  );
  element.setAttribute("download", file);
  element.style.display = "none";
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
}

function functionWellnessFileDownloadData(jsonData) {
  var array = jsonData;
  var options_table = "";

  array.forEach(function (element, i) {
    var CodeNo = element.CodeNo ? element.CodeNo : "";

    options_table += `movie 0:00:00.0 U:\\Content\\${CodeNo}` + "\n";
    if (i == array.length - 1) {
      //initiate for 1st row
      var filedata = options_table;
      var text = filedata;
      download(file, text);
      toastr.success(`${file} \n File Has been Downloaded Successfully.`);
      filedata.empty();
    }
  });
}

// Air file for onetake
function functionOnetakeFileDownloadData(jsonData) {
  console.log("userChannelName", userChannelName, jsonData);
  var array = jsonData;
  var channelName = userChannelName.trim();
  var options_table = "";
  let segmentPartNumber = 1;

  array.forEach(function (element, i) {
    console.log("next", element.Next);
    var ContentID = element.ContentID ? element.ContentID : "-";
    // var ProgramName = element.ProgramName.trim() ? element.ProgramName.trim() : "-";
    var ProgramFile = element.ProgramFile ? element.ProgramFile : "-";
    var Duration = element.Duration ? element.Duration : "-";
    var Type = element.Type ? element.Type : "-";
    var Next = element.Next ? element.Next : "-";
    var SCTE = element.SCTE ? element.SCTE : "-";
    var OutPoint = element.OutPoint ? element.OutPoint : "-";
    var InPoint = element.InPoint ? element.InPoint : "-";
    var PartNo = element.PartNo ? element.PartNo : "-";

    if (channelName === "Kids Toons") {
      if (Next !== "-") {
        options_table +=
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie 00:00:00.00 X:\\kids toons__new\\${ContentID}_${ProgramFile}` +
          "\n";
      } else {
        options_table +=
          `comment 0` +
          "\n" +
          `movie 00:00:00.00 X:\\kids toons__new\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "Cooking") {
      if (Next !== "-") {
        options_table +=
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie 00:00:00.00 X:\\COOKING_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      } else {
        options_table +=
          `comment 0` +
          "\n" +
          `movie 00:00:00.00 X:\\COOKING_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "Kids Rhymes") {
      if (Next !== "-") {
        options_table +=
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie 00:00:00.00  X:\\KIDS RHYMES_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      } else {
        options_table +=
          `comment 0` +
          "\n" +
          `movie 00:00:00.00  X:\\KIDS RHYMES_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "Hollywood Action") {
      if (Next !== "-") {
        options_table +=
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie 00:00:00:00   X:\\HOLLYWOOD ACTION_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      } else {
        options_table +=
          `comment 0` +
          "\n" +
          `movie 00:00:00:00   X:\\HOLLYWOOD ACTION_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "Bhojpuri Songs") {
      if (Next !== "-") {
        options_table +=
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie 00:00:00.00 Y:\\BHOJPURI_NEW\\SONGS\\${ContentID}_${ProgramFile}` +
          "\n";
      } else {
        options_table +=
          `comment 0` +
          "\n" +
          `movie 00:00:00.00 Y:\\BHOJPURI_NEW\\SONGS\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "Bengali Movies") {
      if (Next !== "-") {
        options_table +=
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie 00:00:00:00   X:\\BENGALI_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      } else {
        options_table +=
          `comment 0` +
          "\n" +
          `movie 00:00:00:00   X:\\BENGALI_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "Bhojpuri Movies") {
      if (Next !== "-") {
        options_table +=
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie 00:00:00.00  X:\\BHOJPURI_NEW\\${Type}\\${ContentID}_${ProgramFile}` +
          "\n";
      } else {
        options_table +=
          `comment 0` +
          "\n" +
          `movie 00:00:00.00  X:\\BHOJPURI_NEW\\${Type}\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "Hollywood Gujrati Movies") {
      if (Next !== "-") {
        options_table +=
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie 00:00:00:00   S:\\GUJARATI MOVIES_NEW\\Movies\\${ContentID}_${ProgramFile}` +
          "\n";
      } else {
        options_table +=
          `comment 0` +
          "\n" +
          `movie 00:00:00:00   S:\\GUJARATI MOVIES_NEW\\Movies\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "Hollywood Marathi Movies") {
      if (Next !== "-") {
        options_table +=
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie 00:00:00:00  S:\\MARATHI MOVIE_NEW\\Movies\\${ContentID}_${ProgramFile}` +
          "\n";
      } else {
        options_table +=
          `comment 0` +
          "\n" +
          `movie 00:00:00:00  S:\\MARATHI MOVIE_NEW\\Movies\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "Hollywood Movies") {
      if (Next !== "-") {
        options_table +=
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie 00:00:00:00   Y:\\KCCL_HOLLYWOOD ONE_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      } else {
        options_table +=
          `comment 0` +
          "\n" +
          `movie 00:00:00:00   Y:\\KCCL_HOLLYWOOD ONE_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "Hollywood Tamil") {
      if (Next !== "-") {
        options_table +=
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie 00:00:00:00   Y:\\TAMIL_NEW\\${Type}\\${ContentID}_${ProgramFile}` +
          "\n";
      } else {
        options_table +=
          `comment 0` +
          "\n" +
          `movie 00:00:00:00   Y:\\TAMIL_NEW\\${Type}\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "Hollywood Telugu") {
      if (Next !== "-") {
        options_table +=
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie 00:00:00:00   Y:\\TELUGU_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      } else {
        options_table +=
          `comment 0` +
          "\n" +
          `movie 00:00:00:00   Y:\\TELUGU_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "K-PoP") {
      if (Next !== "-") {
        options_table +=
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie 00:00:00:00   Y:\\KOREAN_NEW\\K-POP\\${ContentID}_${ProgramFile}` +
          "\n";
      } else {
        options_table +=
          `comment 0` +
          "\n" +
          `movie 00:00:00:00   Y:\\KOREAN_NEW\\K-POP\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "K-World") {
      if (Next !== "-") {
        options_table +=
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie 00:00:00:00   X:\\KOREAN_NEW\\Content\\${Type}\\${ContentID}_${ProgramFile}` +
          "\n";
      } else {
        options_table +=
          `comment 0` +
          "\n" +
          `movie 00:00:00:00   X:\\KOREAN_NEW\\Content\\${Type}\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "Kids Movies") {
      if (Next !== "-") {
        options_table +=
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie 00:00:00.00 X:\\KIDS MOVIES_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      } else {
        options_table +=
          `comment 0` +
          "\n" +
          `movie 00:00:00.00 X:\\KIDS MOVIES_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "Kids Toons Marathi") {
      if (Next !== "-") {
        options_table +=
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie 00:00:00.00 S:\\KIDS TOONS MARATHI_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      } else {
        options_table +=
          `comment 0` +
          "\n" +
          `movie 00:00:00.00 S:\\KIDS TOONS MARATHI_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "Malayalam") {
      if (Next !== "-") {
        options_table +=
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie 00:00:00:00   X:\\MALYALAM_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      } else {
        options_table +=
          `comment 0` +
          "\n" +
          `movie 00:00:00:00   X:\\MALYALAM_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "South Action") {
      if (Next !== "-") {
        options_table +=
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie 00:00:00:00   X:\\SOUTH ACTION_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      } else {
        options_table +=
          `comment 0` +
          "\n" +
          `movie 00:00:00:00   X:\\SOUTH ACTION_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "Hollywood Action Bangla") {
      if (Next !== "-") {
        options_table +=
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie 00:00:00:00    X:\\HOLLYWOOD ACTION BANGLA\\${ContentID}_${ProgramFile}` +
          "\n";
      } else {
        options_table +=
          `comment 0` +
          "\n" +
          `movie 00:00:00:00    X:\\HOLLYWOOD ACTION BANGLA\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "Hollywood Action Punjabi") {
      if (Next !== "-") {
        options_table +=
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie 00:00:00:00    x:\\hollywood action punjabi\\${ContentID}_${ProgramFile}` +
          "\n";
      } else {
        options_table +=
          `comment 0` +
          "\n" +
          `movie 00:00:00:00    x:\\hollywood action punjabi\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "Kids Toons Punjabi") {
      if (Next !== "-") {
        options_table +=
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie 00:00:00:00    x:\\KIDS_Toon_Punjabi\\${ContentID}_${ProgramFile}` +
          "\n";
      } else {
        options_table +=
          `comment 0` +
          "\n" +
          `movie 00:00:00:00    x:\\KIDS_Toon_Punjabi\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "Cooking SCTE") {
      if (Next !== "-" && SCTE == "Start ADS") {
        options_table +=
          `comment 0 ${PartNo}` +
          "\n" +
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie <${InPoint}> ${Duration} X:\\COOKING_NEW\\${ContentID}_${ProgramFile}` +
          "\n" +
          `markstart {SCTE35/0} 0 Start ADS` +
          "\n";
      } else if (SCTE == "Stop ADS") {
        // options_table +=    `movie <${InPoint}> ${OutPoint} X:\\COOKING_NEW\\${ContentID}_${ProgramFile}` + "\n" +
        // `markstop {SCTE35} 0 Stop ADS`  + "\n"
        options_table +=
          `movie ${InPoint} X:\\COOKING_NEW\\${ContentID}_${ProgramFile}` +
          "\n" +
          `markstop {SCTE35} 0 Stop ADS` +
          "\n";
      } else {
        // options_table +=    `movie <${InPoint}> ${OutPoint} X:\\COOKING_NEW\\${ContentID}_${ProgramFile}` + "\n"
        options_table +=
          `movie ${InPoint} X:\\COOKING_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "Hollywood Action SCTE") {
      if (Next !== "-" && SCTE == "Start ADS") {
        options_table +=
          `comment 0 ${PartNo}` +
          "\n" +
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie <${InPoint}> ${Duration} X:\\HOLLYWOOD ACTION_NEW\\${ContentID}_${ProgramFile}` +
          "\n" +
          `markstart {SCTE35/0} 0 Start ADS` +
          "\n";
      } else if (SCTE == "Stop ADS") {
        options_table +=
          `movie ${InPoint} X:\\HOLLYWOOD ACTION_NEW\\${ContentID}_${ProgramFile}` +
          "\n" +
          `markstop {SCTE35} 0 Stop ADS` +
          "\n";
      } else {
        options_table +=
          `movie ${InPoint} X:\\HOLLYWOOD ACTION_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      }

      // } else if (channelName === 'Kids Toons SCTE') {
    } else if (channelName === "Comedy King SCTE") {
      console.log("in Kids Toons SCTE");
      if (Next !== "-" && SCTE == "Start ADS") {
        options_table +=
          `comment 0 ${PartNo}` +
          "\n" +
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie <${InPoint}> ${Duration} X:\\COMEDY KING\\${ContentID}_${ProgramFile}` +
          "\n" +
          `markstart {SCTE35/0} 0 Start ADS` +
          "\n";
      } else if (SCTE == "Stop ADS") {
        options_table +=
          `movie ${InPoint} X:\\COMEDY KING\\${ContentID}_${ProgramFile}` +
          "\n" +
          `markstop {SCTE35} 0 Stop ADS` +
          "\n";
      } else {
        options_table +=
          `movie ${InPoint} X:\\COMEDY KING\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "K-World SCTE") {
      if (Next !== "-" && SCTE == "Start ADS") {
        options_table +=
          `comment 0 ${PartNo}` +
          "\n" +
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie <${InPoint}> ${Duration} X:\\K-Word Fast TV\\${Type}\\${ContentID}_${ProgramFile}` +
          "\n" +
          `markstart {SCTE35/0} 0 Start ADS` +
          "\n";
      } else if (SCTE == "Stop ADS") {
        options_table +=
          `movie ${InPoint} X:\\K-Word Fast TV\\${Type}\\${ContentID}_${ProgramFile}` +
          "\n" +
          `markstop {SCTE35} 0 Stop ADS` +
          "\n";
      } else {
        options_table +=
          `movie ${InPoint} X:\\K-Word Fast TV\\${Type}\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "Hollywood Desi SCTE") {
      if (Next !== "-" && SCTE == "Start ADS") {
        options_table +=
          `comment 0 ${PartNo}` +
          "\n" +
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie <${InPoint}> ${Duration} X:\\HOLLYWOOD ACTION_NEW\\${ContentID}_${ProgramFile}` +
          "\n" +
          `markstart {SCTE35/0} 0 Start ADS` +
          "\n";
      } else if (SCTE == "Stop ADS") {
        options_table +=
          `movie ${InPoint} X:\\HOLLYWOOD ACTION_NEW\\${ContentID}_${ProgramFile}` +
          "\n" +
          `markstop {SCTE35} 0 Stop ADS` +
          "\n";
      } else {
        options_table +=
          `movie ${InPoint} X:\\HOLLYWOOD ACTION_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      }

      // } else if (channelName === 'Hollywood Movies SCTE') {
    } else if (channelName === "Bhakti Play SCTE") {
      if (Next !== "-" && SCTE == "Start ADS") {
          options_table +=
          `comment 0 ${PartNo}` +
          "\n" +
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie <${InPoint}> ${Duration} x:\\bhakti play\\${ContentID}_${ProgramFile}` +
          "\n" +
          `markstart {SCTE35/0} 0 Start ADS` +
          "\n";
        
      } else if (SCTE == "Stop ADS") {
        options_table +=
          `movie ${InPoint} x:\\bhakti play\\${ContentID}_${ProgramFile}` +
          "\n" +
          `markstop {SCTE35} 0 Stop ADS` +
          "\n";
      }  
      else {
        if(SCTE === "-" && PartNo !== "-"){
          options_table +=
          `comment 0 ${PartNo}` +
          "\n" +
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie <${InPoint}> ${Duration} x:\\bhakti play\\${ContentID}_${ProgramFile}` +
          "\n"
        }else{
          options_table +=

          `movie ${InPoint} x:\\bhakti play\\${ContentID}_${ProgramFile}` +
          "\n";
        }
        
      }
    } else if (channelName === "Hooray Rhymes SCTE") {
      if (Next !== "-" && SCTE == "Start ADS") {
        options_table +=
          `comment 0 ${PartNo}` +
          "\n" +
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie <${InPoint}> ${Duration} X:\\HOORAY RHYMES\\${ContentID}_${ProgramFile}` +
          "\n" +
          `markstart {SCTE35/0} 0 Start ADS` +
          "\n";
      } else if (SCTE == "Stop ADS") {
        options_table +=
          `movie ${InPoint} X:\\HOORAY RHYMES\\${ContentID}_${ProgramFile}` +
          "\n" +
          `markstop {SCTE35} 0 Stop ADS` +
          "\n";
      } else {
        options_table +=
          `movie ${InPoint} X:\\HOORAY RHYMES\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "Hooray Kids SCTE") {
      if (Next !== "-" && SCTE == "Start ADS") {
        options_table +=
          `comment 0 ${PartNo}` +
          "\n" +
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie <${InPoint}> ${Duration} X:\\HOORAY_KIDS_NEW\\${ContentID}_${ProgramFile}` +
          "\n" +
          `markstart {SCTE35/0} 0 Start ADS` +
          "\n";
      } else if (SCTE == "Stop ADS") {
        options_table +=
          `movie ${InPoint} X:\\HOORAY_KIDS_NEW\\${ContentID}_${ProgramFile}` +
          "\n" +
          `markstop {SCTE35} 0 Stop ADS` +
          "\n";
      } else {
        options_table +=
          `movie ${InPoint} X:\\HOORAY_KIDS_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "Kiddo Matix SCTE") {
      if (Next !== "-" && SCTE == "Start ADS") {
        options_table +=
          `comment 0 ${PartNo}` +
          "\n" +
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie <${InPoint}> ${Duration} X:\\Kiddo_Matrix_New\\${ContentID}_${ProgramFile}` +
          "\n" +
          `markstart {SCTE35/0} 0 Start ADS` +
          "\n";
      } else if (SCTE == "Stop ADS") {
        options_table +=
          `movie ${InPoint} X:\\Kiddo_Matrix_New\\${ContentID}_${ProgramFile}` +
          "\n" +
          `markstop {SCTE35} 0 Stop ADS` +
          "\n";
      } else {
        options_table +=
          `movie ${InPoint} X:\\Kiddo_Matrix_New\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "Comedy Club") {
      if (Next !== "-") {
        options_table +=
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie 00:00:00.00 X:\\COMEDY KING\\${ContentID}_${ProgramFile}` +
          "\n";
      } else {
        options_table +=
          `comment 0` +
          "\n" +
          `movie 00:00:00.00 X:\\COMEDY KING\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "Bhakti Play") {
      if (Next !== "-") {
        options_table +=
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie 00:00:00.00 X:\\BHAKTI PLAY\\${ContentID}_${ProgramFile}` +
          "\n";
      } else {
        options_table +=
          `comment 0` +
          "\n" +
          `movie 00:00:00.00 X:\\BHAKTI PLAY\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "K-World(Telugu)") {
      if (Next !== "-") {
        options_table +=
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie 00:00:00.00 X:\\KOREAN_TELUGU_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      } else {
        options_table +=
          `comment 0` +
          "\n" +
          `movie 00:00:00.00 X:\\KOREAN_TELUGU_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    } else if (channelName === "K-World(Tamil)") {
      if (Next !== "-") {
        options_table +=
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          `movie 00:00:00.00 X:\\KOREAN_TAMIL_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      } else {
        options_table +=
          `comment 0` +
          "\n" +
          `movie 00:00:00.00 X:\\KOREAN_TAMIL_NEW\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    }

    if (i == array.length - 1) {
      var filedata = options_table;
      var text = filedata;
      download(file, text);
      toastr.success(`${file} \n File Has been Downloaded Successfully.`);
    }
  });
}

//! Air file for Pitaara
function functionPitaaraFileDownloadDataAir(jsonData) {
  // console.log("jsonData---",jsonData)
  var array = jsonData;
  var options_table = "";
  var channelName = userChannelName.trim();

  let scteCommFiles = "";
  let scteInitialCommFile = 0;
  const regexFilter = new RegExp(/^.{0}(COM)/);

  console.log("Channel name pitaara air", channelName);
  if (channelName === "Pitaara India") {
    array.forEach(function (element, i) {
      // console.log("element--",element)

      var SCTE = element.SCTE ? element.SCTE : "-";
      var TAPE_ID = element["TAPE ID"] ? element["TAPE ID"] : "-";
      //replace location starting with "V" to "U"
      var Location = element.Location ? element.Location : "-";
      let locationWithPathChange = "";
      if (Location !== "-") {
        locationWithPathChange = Location.replace(Location[0], "V");
      } else {
        locationWithPathChange = Location;
      }

      if (SCTE == "Start ADS") {
        options_table +=
          `movie 00:00:00.00 V:\\SAMSUNG & COMEDY DATA\\${TAPE_ID}.mp4` +
          "\n" +
          `markstart {SCTE35/0} 0 Start ADS` +
          "\n";
      } else if (SCTE == "Stop ADS") {
        options_table +=
          `movie 00:00:00.00 V:\\SAMSUNG & COMEDY DATA\\${TAPE_ID}.mp4` +
          "\n" +
          `markstop {SCTE35} 0 Stop ADS` +
          "\n";
      } else {
        options_table += `movie 00:00:00.00 V:\\SAMSUNG & COMEDY DATA\\${TAPE_ID}.mp4` + "\n";
      }
      /*
            if (regexFilter.test(element["TAPE ID"]) && scteInitialCommFile === 0) {
                // initial file COM file
                scteInitialCommFile++
                scteCommFiles += `markstart {SCTE35/0} 0 Start ADS` + "\n";
                scteCommFiles += `movie 0:00:00.0 ${locationWithPathChange}` + "\n";
            } else if (regexFilter.test(element["TAPE ID"]) && scteInitialCommFile > 0) {
                //normal COM file
                scteCommFiles += `movie 0:00:00.0 ${locationWithPathChange}` + "\n";

            } else if (scteInitialCommFile > 0 && scteInitialCommFile != 0) {
                // COM files are done. now all normal files
                options_table += scteCommFiles
                options_table += `markstop {SCTE35} 0 Stop ADS` + "\n";
                options_table += `movie 0:00:00.0 ${locationWithPathChange}` + "\n";
                scteCommFiles = ""
                scteInitialCommFile = 0
            } else {
                console.log("Else options --------", element["TAPE ID"])
                options_table += `movie 0:00:00.0 ${locationWithPathChange}` + "\n";
            }
            */

      // options_table += `movie 0:00:00.0 ${locationWithPathChange}` + "\n";
      if (i == array.length - 1) {
        //initiate for 1st row
        var filedata = options_table;
        var text = filedata;
        download(file, text);
        toastr.success(`${file} \n File Has been Downloaded Successfully.`);
        console.log("File Data", typeof filedata);
        filedata = "";
        // filedata.empty();
      }
    });
  } else if (channelName === "Pitaara UK") {
    array.forEach(function (element, i) {
      // console.log("element--",element)
      //replace location starting with "V" to "U"

      var SCTE = element.SCTE ? element.SCTE : "-";
      var TAPE_ID = element["TAPE ID"] ? element["TAPE ID"] : "-";
      var Location = element.Location ? element.Location : "-";
      let locationWithPathChange = "";
      if (Location !== "-") {
        locationWithPathChange = `V:\\SAMSUNG & COMEDY DATA\\${element["TAPE ID"]}.mp4`;
      } else {
        locationWithPathChange = Location;
      }

      if (SCTE == "Start ADS") {
        options_table +=
          `movie 00:00:00.00 V:\\SAMSUNG & COMEDY DATA\\${TAPE_ID}.mp4` +
          "\n" +
          `markstart {SCTE35/0} 0 Start ADS` +
          "\n";
      } else if (SCTE == "Stop ADS") {
        options_table +=
          `movie 00:00:00.00 V:\\SAMSUNG & COMEDY DATA\\${TAPE_ID}.mp4` +
          "\n" +
          `markstop {SCTE35} 0 Stop ADS` +
          "\n";
      } else {
        options_table += `movie 00:00:00.00 V:\\SAMSUNG & COMEDY DATA\\${TAPE_ID}.mp4` + "\n";
      }

      /* if ((element["Category"] === "UK COMMERCIAL" || element["Category"] === "COMM") && scteInitialCommFile === 0) {
                 // initial file COM file
                 scteInitialCommFile++
                 scteCommFiles += `markstart {SCTE35/0} 0 Start ADS` + "\n";
                 scteCommFiles += `movie 0:00:00.0 ${locationWithPathChange}` + "\n";
             } else if ((element["Category"] === "UK COMMERCIAL" || element["Category"] === "COMM") && scteInitialCommFile > 0) {
                 //normal COM file
                 scteCommFiles += `movie 0:00:00.0 ${locationWithPathChange}` + "\n";

             } else if (scteInitialCommFile > 0 && scteInitialCommFile != 0) {
                 // COM files are done. now all normal files
                 options_table += scteCommFiles
                 options_table += `markstop {SCTE35} 0 Stop ADS` + "\n";
                 options_table += `movie 0:00:00.0 ${locationWithPathChange}` + "\n";
                 scteCommFiles = ""
                 scteInitialCommFile = 0
             } else {
                 console.log("Else options --------", element["TAPE ID"])
                 options_table += `movie 0:00:00.0 ${locationWithPathChange}` + "\n";
             }
             */

      // options_table += `movie 0:00:00.0 ${locationWithPathChange}` + "\n";
      if (i == array.length - 1) {
        //initiate for 1st row
        var filedata = options_table;
        var text = filedata;
        download(file, text);
        toastr.success(`${file} \n File Has been Downloaded Successfully.`);
        console.log("File Data", typeof filedata);
        filedata = "";
        // filedata.empty();
      }
    });
  } else if (channelName === "Pitaara DIVYA") {
    console.log("Pitaara DIVYA");
    array.forEach(function (element, i) {
      // console.log("element--",element)
      //replace location starting with "V" to "U"

      var SCTE = element.SCTE ? element.SCTE : "-";
      // var TAPE_ID = element["TAPE ID"] ? element["TAPE ID"] : "-";
      var Location = element.Location ? element.Location : "-";
      // let locationWithPathChange = ""
      // if (Location !== "-") {
      //     locationWithPathChange = `V:\\${element["TAPE ID"]}`
      // } else {
      //     locationWithPathChange = Location
      // }

      if (SCTE == "Start ADS") {
        options_table +=
          `movie 00:00:00.00 ${Location}` +
          "\n" +
          `markstart {SCTE35/0} 0 Start ADS` +
          "\n";
      } else if (SCTE == "Stop ADS") {
        options_table +=
          `movie 00:00:00.00 ${Location}` +
          "\n" +
          `markstop {SCTE35} 0 Stop ADS` +
          "\n";
      } else {
        options_table += `movie 00:00:00.00 ${Location}` + "\n";
      }

      /* if ((element["Category"] === "UK COMMERCIAL" || element["Category"] === "COMM") && scteInitialCommFile === 0) {
                 // initial file COM file
                 scteInitialCommFile++
                 scteCommFiles += `markstart {SCTE35/0} 0 Start ADS` + "\n";
                 scteCommFiles += `movie 0:00:00.0 ${locationWithPathChange}` + "\n";
             } else if ((element["Category"] === "UK COMMERCIAL" || element["Category"] === "COMM") && scteInitialCommFile > 0) {
                 //normal COM file
                 scteCommFiles += `movie 0:00:00.0 ${locationWithPathChange}` + "\n";

             } else if (scteInitialCommFile > 0 && scteInitialCommFile != 0) {
                 // COM files are done. now all normal files
                 options_table += scteCommFiles
                 options_table += `markstop {SCTE35} 0 Stop ADS` + "\n";
                 options_table += `movie 0:00:00.0 ${locationWithPathChange}` + "\n";
                 scteCommFiles = ""
                 scteInitialCommFile = 0
             } else {
                 console.log("Else options --------", element["TAPE ID"])
                 options_table += `movie 0:00:00.0 ${locationWithPathChange}` + "\n";
             }
             */

      // options_table += `movie 0:00:00.0 ${locationWithPathChange}` + "\n";
      if (i == array.length - 1) {
        //initiate for 1st row
        var filedata = options_table;
        var text = filedata;
        download(file, text);
        toastr.success(`${file} \n File Has been Downloaded Successfully.`);
        console.log("File Data", typeof filedata);
        filedata = "";
        // filedata.empty();
      }
    });
  }
}

//! Air file for GreenChillies
function functionFileDownloadDataGreenChillies(jsonData) {
  var array = jsonData;
  var options_table = "";
  array.forEach(function (element, i) {
    var Event = element.Event ? element.Event.trim() : "-";
    let ClipId = element["Clip ID"]?.trim() ? element["Clip ID"].trim() : "-";

    options_table +=
      `movie 0:00:00.0 G:\\Content\\${ClipId}_${Event}.mov` + "\n";
    if (i == array.length - 1) {
      //initiate for 1st row
      var filedata = options_table;
      var text = filedata;
      download(file, text);
      toastr.success(`${file} \n File Has been Downloaded Successfully.`);
      filedata = "";
    }
  });
}

//! Air file for Fancode
function functionFancodeFileDownloadData(jsonData) {
  console.log("userChannelName", userChannelName, jsonData);
  var array = jsonData;
  var channelName = userChannelName.trim();
  var options_table = "";
  let segmentPartNumber = 1;

  array.forEach(function (element, i) {
    console.log("next", element.Next);
    var ContentID = element.ContentID ? element.ContentID : "-";
    // var ProgramName = element.ProgramName.trim() ? element.ProgramName.trim() : "-";
    var ProgramFile = element.ProgramFile ? element.ProgramFile : "-";
    var Duration = element.Duration ? element.Duration : "-";
    var Type = element.Type ? element.Type : "-";
    var Next = element.Next ? element.Next : "-";
    var SCTE = element.SCTE ? element.SCTE : "-";
    var OutPoint = element.OutPoint ? element.OutPoint : "-";
    var InPoint = element.InPoint ? element.InPoint : "-";
    var PartNo = element.PartNo ? element.PartNo : "-";

    if (channelName === "Fancode Fast Channel") {
      // if (Next !== "-")
      if (Next !== "-" && ContentID === "Live") {
        options_table +=
          `comment 0 # <ST 0>${Next}` +
          "\n" +
          // `movie 00:00:00.00 d:\\content\\${ContentID}` +
          // "\n" +
          `video1 00:00:01.00 [0.1]` +
          "\n";
        // `video1 0` +
        // "\n";
      } else {
        options_table +=
          `comment 0` +
          "\n" +
          `movie 00:00:00.00 d:\\content\\${ContentID}_${ProgramFile}` +
          "\n";
      }
    }
    // else if (channelName === "Cooking") {
    //   if (Next !== "-") {
    //     options_table +=
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie 00:00:00.00 X:\\COOKING_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `comment 0` +
    //       "\n" +
    //       `movie 00:00:00.00 X:\\COOKING_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "Kids Rhymes") {
    //   if (Next !== "-") {
    //     options_table +=
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie 00:00:00.00  X:\\KIDS RHYMES_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `comment 0` +
    //       "\n" +
    //       `movie 00:00:00.00  X:\\KIDS RHYMES_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "Hollywood Action") {
    //   if (Next !== "-") {
    //     options_table +=
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie 00:00:00:00   X:\\HOLLYWOOD ACTION_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `comment 0` +
    //       "\n" +
    //       `movie 00:00:00:00   X:\\HOLLYWOOD ACTION_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "Bhojpuri Songs") {
    //   if (Next !== "-") {
    //     options_table +=
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie 00:00:00.00 Y:\\BHOJPURI_NEW\\SONGS\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `comment 0` +
    //       "\n" +
    //       `movie 00:00:00.00 Y:\\BHOJPURI_NEW\\SONGS\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "Bengali Movies") {
    //   if (Next !== "-") {
    //     options_table +=
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie 00:00:00:00   X:\\BENGALI_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `comment 0` +
    //       "\n" +
    //       `movie 00:00:00:00   X:\\BENGALI_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "Bhojpuri Movies") {
    //   if (Next !== "-") {
    //     options_table +=
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie 00:00:00.00  X:\\BHOJPURI_NEW\\${Type}\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `comment 0` +
    //       "\n" +
    //       `movie 00:00:00.00  X:\\BHOJPURI_NEW\\${Type}\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "Hollywood Gujrati Movies") {
    //   if (Next !== "-") {
    //     options_table +=
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie 00:00:00:00   S:\\GUJARATI MOVIES_NEW\\Movies\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `comment 0` +
    //       "\n" +
    //       `movie 00:00:00:00   S:\\GUJARATI MOVIES_NEW\\Movies\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "Hollywood Marathi Movies") {
    //   if (Next !== "-") {
    //     options_table +=
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie 00:00:00:00  S:\\MARATHI MOVIE_NEW\\Movies\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `comment 0` +
    //       "\n" +
    //       `movie 00:00:00:00  S:\\MARATHI MOVIE_NEW\\Movies\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "Hollywood Movies") {
    //   if (Next !== "-") {
    //     options_table +=
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie 00:00:00:00   Y:\\KCCL_HOLLYWOOD ONE_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `comment 0` +
    //       "\n" +
    //       `movie 00:00:00:00   Y:\\KCCL_HOLLYWOOD ONE_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "Hollywood Tamil") {
    //   if (Next !== "-") {
    //     options_table +=
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie 00:00:00:00   Y:\\TAMIL_NEW\\${Type}\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `comment 0` +
    //       "\n" +
    //       `movie 00:00:00:00   Y:\\TAMIL_NEW\\${Type}\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "Hollywood Telugu") {
    //   if (Next !== "-") {
    //     options_table +=
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie 00:00:00:00   Y:\\TELUGU_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `comment 0` +
    //       "\n" +
    //       `movie 00:00:00:00   Y:\\TELUGU_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "K-PoP") {
    //   if (Next !== "-") {
    //     options_table +=
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie 00:00:00:00   Y:\\KOREAN_NEW\\K-POP\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `comment 0` +
    //       "\n" +
    //       `movie 00:00:00:00   Y:\\KOREAN_NEW\\K-POP\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "K-World") {
    //   if (Next !== "-") {
    //     options_table +=
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie 00:00:00:00   X:\\KOREAN_NEW\\Content\\${Type}\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `comment 0` +
    //       "\n" +
    //       `movie 00:00:00:00   X:\\KOREAN_NEW\\Content\\${Type}\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "Kids Movies") {
    //   if (Next !== "-") {
    //     options_table +=
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie 00:00:00.00 X:\\KIDS MOVIES_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `comment 0` +
    //       "\n" +
    //       `movie 00:00:00.00 X:\\KIDS MOVIES_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "Kids Toons Marathi") {
    //   if (Next !== "-") {
    //     options_table +=
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie 00:00:00.00 S:\\KIDS TOONS MARATHI_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `comment 0` +
    //       "\n" +
    //       `movie 00:00:00.00 S:\\KIDS TOONS MARATHI_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "Malayalam") {
    //   if (Next !== "-") {
    //     options_table +=
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie 00:00:00:00   X:\\MALYALAM_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `comment 0` +
    //       "\n" +
    //       `movie 00:00:00:00   X:\\MALYALAM_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "South Action") {
    //   if (Next !== "-") {
    //     options_table +=
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie 00:00:00:00   X:\\SOUTH ACTION_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `comment 0` +
    //       "\n" +
    //       `movie 00:00:00:00   X:\\SOUTH ACTION_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "Hollywood Action Bangla") {
    //   if (Next !== "-") {
    //     options_table +=
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie 00:00:00:00    X:\\HOLLYWOOD ACTION BANGLA\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `comment 0` +
    //       "\n" +
    //       `movie 00:00:00:00    X:\\HOLLYWOOD ACTION BANGLA\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "Hollywood Action Punjabi") {
    //   if (Next !== "-") {
    //     options_table +=
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie 00:00:00:00    x:\\hollywood action punjabi\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `comment 0` +
    //       "\n" +
    //       `movie 00:00:00:00    x:\\hollywood action punjabi\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "Kids Toons Punjabi") {
    //   if (Next !== "-") {
    //     options_table +=
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie 00:00:00:00    x:\\KIDS_Toon_Punjabi\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `comment 0` +
    //       "\n" +
    //       `movie 00:00:00:00    x:\\KIDS_Toon_Punjabi\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "Cooking SCTE") {
    //   if (Next !== "-" && SCTE == "Start ADS") {
    //     options_table +=
    //       `comment 0 ${PartNo}` +
    //       "\n" +
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie <${InPoint}> ${Duration} X:\\COOKING_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n" +
    //       `markstart {SCTE35/0} 0 Start ADS` +
    //       "\n";
    //   } else if (SCTE == "Stop ADS") {
    //     // options_table +=    `movie <${InPoint}> ${OutPoint} X:\\COOKING_NEW\\${ContentID}_${ProgramFile}` + "\n" +
    //     // `markstop {SCTE35} 0 Stop ADS`  + "\n"
    //     options_table +=
    //       `movie ${InPoint} X:\\COOKING_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n" +
    //       `markstop {SCTE35} 0 Stop ADS` +
    //       "\n";
    //   } else {
    //     // options_table +=    `movie <${InPoint}> ${OutPoint} X:\\COOKING_NEW\\${ContentID}_${ProgramFile}` + "\n"
    //     options_table +=
    //       `movie ${InPoint} X:\\COOKING_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "Hollywood Action SCTE") {
    //   if (Next !== "-" && SCTE == "Start ADS") {
    //     options_table +=
    //       `comment 0 ${PartNo}` +
    //       "\n" +
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie <${InPoint}> ${Duration} X:\\HOLLYWOOD ACTION_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n" +
    //       `markstart {SCTE35/0} 0 Start ADS` +
    //       "\n";
    //   } else if (SCTE == "Stop ADS") {
    //     options_table +=
    //       `movie ${InPoint} X:\\HOLLYWOOD ACTION_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n" +
    //       `markstop {SCTE35} 0 Stop ADS` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `movie ${InPoint} X:\\HOLLYWOOD ACTION_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }

    //   // } else if (channelName === 'Kids Toons SCTE') {
    // } else if (channelName === "Comedy King SCTE") {
    //   console.log("in Kids Toons SCTE");
    //   if (Next !== "-" && SCTE == "Start ADS") {
    //     options_table +=
    //       `comment 0 ${PartNo}` +
    //       "\n" +
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie <${InPoint}> ${Duration} X:\\COMEDY KING\\${ContentID}_${ProgramFile}` +
    //       "\n" +
    //       `markstart {SCTE35/0} 0 Start ADS` +
    //       "\n";
    //   } else if (SCTE == "Stop ADS") {
    //     options_table +=
    //       `movie ${InPoint} X:\\COMEDY KING\\${ContentID}_${ProgramFile}` +
    //       "\n" +
    //       `markstop {SCTE35} 0 Stop ADS` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `movie ${InPoint} X:\\COMEDY KING\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "K-World SCTE") {
    //   if (Next !== "-" && SCTE == "Start ADS") {
    //     options_table +=
    //       `comment 0 ${PartNo}` +
    //       "\n" +
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie <${InPoint}> ${Duration} X:\\K-Word Fast TV\\${Type}\\${ContentID}_${ProgramFile}` +
    //       "\n" +
    //       `markstart {SCTE35/0} 0 Start ADS` +
    //       "\n";
    //   } else if (SCTE == "Stop ADS") {
    //     options_table +=
    //       `movie ${InPoint} X:\\K-Word Fast TV\\${Type}\\${ContentID}_${ProgramFile}` +
    //       "\n" +
    //       `markstop {SCTE35} 0 Stop ADS` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `movie ${InPoint} X:\\K-Word Fast TV\\${Type}\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "Hollywood Desi SCTE") {
    //   if (Next !== "-" && SCTE == "Start ADS") {
    //     options_table +=
    //       `comment 0 ${PartNo}` +
    //       "\n" +
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie <${InPoint}> ${Duration} X:\\HOLLYWOOD ACTION_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n" +
    //       `markstart {SCTE35/0} 0 Start ADS` +
    //       "\n";
    //   } else if (SCTE == "Stop ADS") {
    //     options_table +=
    //       `movie ${InPoint} X:\\HOLLYWOOD ACTION_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n" +
    //       `markstop {SCTE35} 0 Stop ADS` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `movie ${InPoint} X:\\HOLLYWOOD ACTION_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }

    //   // } else if (channelName === 'Hollywood Movies SCTE') {
    // } else if (channelName === "Bhakti Play SCTE") {
    //   if (Next !== "-" && SCTE == "Start ADS") {
    //     options_table +=
    //       `comment 0 ${PartNo}` +
    //       "\n" +
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie <${InPoint}> ${Duration} Y:\\KCCL_HOLLYWOOD ONE_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n" +
    //       `markstart {SCTE35/0} 0 Start ADS` +
    //       "\n";
    //   } else if (SCTE == "Stop ADS") {
    //     options_table +=
    //       `movie ${InPoint} Y:\\KCCL_HOLLYWOOD ONE_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n" +
    //       `markstop {SCTE35} 0 Stop ADS` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `movie ${InPoint} Y:\\KCCL_HOLLYWOOD ONE_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "Hooray Rhymes SCTE") {
    //   if (Next !== "-" && SCTE == "Start ADS") {
    //     options_table +=
    //       `comment 0 ${PartNo}` +
    //       "\n" +
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie <${InPoint}> ${Duration} X:\\HOORAY RHYMES\\${ContentID}_${ProgramFile}` +
    //       "\n" +
    //       `markstart {SCTE35/0} 0 Start ADS` +
    //       "\n";
    //   } else if (SCTE == "Stop ADS") {
    //     options_table +=
    //       `movie ${InPoint} X:\\HOORAY RHYMES\\${ContentID}_${ProgramFile}` +
    //       "\n" +
    //       `markstop {SCTE35} 0 Stop ADS` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `movie ${InPoint} X:\\HOORAY RHYMES\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "Hooray Kids SCTE") {
    //   if (Next !== "-" && SCTE == "Start ADS") {
    //     options_table +=
    //       `comment 0 ${PartNo}` +
    //       "\n" +
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie <${InPoint}> ${Duration} X:\\HOORAY_KIDS_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n" +
    //       `markstart {SCTE35/0} 0 Start ADS` +
    //       "\n";
    //   } else if (SCTE == "Stop ADS") {
    //     options_table +=
    //       `movie ${InPoint} X:\\HOORAY_KIDS_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n" +
    //       `markstop {SCTE35} 0 Stop ADS` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `movie ${InPoint} X:\\HOORAY_KIDS_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "Kiddo Matix SCTE") {
    //   if (Next !== "-" && SCTE == "Start ADS") {
    //     options_table +=
    //       `comment 0 ${PartNo}` +
    //       "\n" +
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie <${InPoint}> ${Duration} X:\\Kiddo_Matrix_New\\${ContentID}_${ProgramFile}` +
    //       "\n" +
    //       `markstart {SCTE35/0} 0 Start ADS` +
    //       "\n";
    //   } else if (SCTE == "Stop ADS") {
    //     options_table +=
    //       `movie ${InPoint} X:\\Kiddo_Matrix_New\\${ContentID}_${ProgramFile}` +
    //       "\n" +
    //       `markstop {SCTE35} 0 Stop ADS` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `movie ${InPoint} X:\\Kiddo_Matrix_New\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "Comedy Club") {
    //   if (Next !== "-") {
    //     options_table +=
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie 00:00:00.00 X:\\COMEDY KING\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `comment 0` +
    //       "\n" +
    //       `movie 00:00:00.00 X:\\COMEDY KING\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "Bhakti Play") {
    //   if (Next !== "-") {
    //     options_table +=
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie 00:00:00.00 X:\\BHAKTI PLAY\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `comment 0` +
    //       "\n" +
    //       `movie 00:00:00.00 X:\\BHAKTI PLAY\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "K-World(Telugu)") {
    //   if (Next !== "-") {
    //     options_table +=
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie 00:00:00.00 X:\\KOREAN_TELUGU_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `comment 0` +
    //       "\n" +
    //       `movie 00:00:00.00 X:\\KOREAN_TELUGU_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // } else if (channelName === "K-World(Tamil)") {
    //   if (Next !== "-") {
    //     options_table +=
    //       `comment 0 # <ST 0>${Next}` +
    //       "\n" +
    //       `movie 00:00:00.00 X:\\KOREAN_TAMIL_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   } else {
    //     options_table +=
    //       `comment 0` +
    //       "\n" +
    //       `movie 00:00:00.00 X:\\KOREAN_TAMIL_NEW\\${ContentID}_${ProgramFile}` +
    //       "\n";
    //   }
    // }

    if (i == array.length - 1) {
      var filedata = options_table;
      var text = filedata;
      download(file, text);
      toastr.success(`${file} \n File Has been Downloaded Successfully.`);
    }
  });
}

function functionFileDownloadDataSrmd(jsonData) {
  var array = jsonData;
  var options_table = "";
  array.forEach(function (element, i) {
    var EventID = element.EventID ? element.EventID : "-";

    options_table +=
      `movie 0:00:00.0 W:\\Raw_Content\\Content\\${EventID}` + "\n";
    if (i == array.length - 1) {
      //initiate for 1st row
      var filedata = options_table;
      var text = filedata;
      download(file, text);
      toastr.success(`${file} \n File Has been Downloaded Successfully.`);
      filedata.empty();
    }
  });
}

function functionFileDownloadData(jsonData) {
  var array = jsonData;
  var options_table = "";

  array.forEach(function (element, i) {
    var line = element ? element : "";

    options_table += `${line}` + "\n";
    if (i == array.length - 1) {
      //initiate for 1st row
      var filedata = options_table;
      var text = filedata;
      download(file, text);
      toastr.success(`${file} \n File Has been Downloaded Successfully.`);
      filedata = "";
    }
  });
}

function getfilename(jio) {
  return new Promise((resolve, reject) => {
    $.post(
      "/v1/fileManagement/getHarekrishnaProgramName",
      jio,
      function (data, status) {
        if (data.status == 200) {
          if (data.data[0].fileData.length > 0) {
            var filenameFillers11 = data.data[0].fileData[0].FileNamewithID
              ? data.data[0].fileData[0].FileNamewithID
              : "-";
            resolve(filenameFillers11);
          } else if (
            data.data[0].fileData.length == 0 ||
            data.data.fileData === undefined
          ) {
            toastr.error("Some ID missing in the file");
          }
        }
      }
    );
  });
}

function fillervalue(fillersparams) {
  return new Promise((resolve, reject) => {
    $.post(
      "/v1/fileManagement/getHarekrishnaProgramName",
      fillersparams,
      function (data, status) {
        if (data.status == 200) {
          if (data.data[0].fileData.length > 0) {
            var filenameFillers = data.data[0].fileData[0].FileNamewithID
              ? data.data[0].fileData[0].FileNamewithID
              : "-";
            resolve(filenameFillers);
          } else if (
            data.data[0].fileData.length == 0 ||
            data.data.fileData === undefined
          ) {
            toastr.error("Some Filler ID missing in the file");
          }
        }
      }
    );
  });
}

async function fillers(jsonData) {
  // <markstart mark="SCTE35" markid="0" title="Start ADS" />
  // <markstop mark="SCTE35" title="Stop ADS" />

  var harekrishnaChannelName = hareuserChannelName.trim();

  var array = jsonData;

  var arraylenght = array.length;

  var options_table = [];
  var str = ``;
  var i = 0;
  // console.log("INSIDE FILLERS HAREKRISHNA",array)
  // console.log("INSIDE FILLERS HAREKRISHNA2",array[1]["ProgramName"])

  // new for loop code for testing -----------------------------------
  // for (let i = 0; i < 1; i++) {

  //     var ProgramName = array[i]["ProgramName"] ? array[i]["ProgramName"] : "-";
  //     var FillersSerialNumber = array[i]["FillersSerialNumber"] ? array[i]["FillersSerialNumber"] : "-";
  //     var ScteFillersSerialNumber = array[i]["ScteOnFillers"] ? array[i]["ScteOnFillers"] : "-";
  //     var FileName = array[i]["FileName"] ? array[i]["FileName"] : "-";
  //     var NextTime = array[i]["EndTime/NextTime"] ? array[i]["EndTime/NextTime"] : "-";
  //     var Next = array[i]["Next"] ? array[i]["Next"] : "-";
  //     var fillers = FillersSerialNumber.split(',') ? FillersSerialNumber.split(',') : "-";
  //     var scteFillers = ScteFillersSerialNumber.split(',') && ScteFillersSerialNumber != "-" ? ScteFillersSerialNumber.split(',') : [];
  //     console.log("SCTE FILLERS --------", scteFillers);
  //     var jio = {
  //         File_Name: FileName.trim()
  //     }
  //     const promise1 = await getfilename(jio); //important

  //     if (harekrishnaChannelName === 'Hare Krishna Tv Pravachan') {

  //         if (jio.File_Name === '00000') {
  //             str += `comment 0 # V:\\Pravachan Content\\${promise1}|0:00:0.00|<ST 0> ${ProgramName}` + "\n" +

  //                 `video1 00:00:01.00 [0.1]` + "\n"
  //         } else {
  //             str += `comment 0 # V:\\Pravachan Content\\${promise1}|0:00:0.00|<ST 0> ${ProgramName}` + "\n" +

  //                 `movie 00:00:00.00 V:\\Pravachan Content\\${promise1}` + "\n"
  //         }
  //     } if (harekrishnaChannelName === 'Hare Krishna Tv') {

  //         //New hare requirement (Adding scte markers on fillers in hare krishna tv)

  //         if (jio.File_Name === '00000') {

  //             str += `comment 0 Slot ${i}` + "\n" +

  //                 `comment 0 # <ST 0>            ${NextTime}|${Next}` + "\n" +

  //                 `video1 00:00:01.00 [0.1]` + "\n"
  //         } else {

  //             if (array[i]["ScrollDetails"] != undefined && array[i]["ScrollDetails"] != null && array[i]["ScrollDetails"] == 'Scroll Off') {

  //                 str += `comment 0 Slot ${i}` + "\n" +

  //                     `comment 0 # <ST 0>            ${NextTime}|${Next}` + "\n" +

  //                     `titleObjAbort {TSF_Ticker_Scroll} 0` + "\n" +
  //                     `movie 00:00:00.00 V:\\Content\\${promise1}` + "\n" +
  //                     `titleObjOn {TSF_Ticker_Scroll} 0`

  //             } else {

  //                 str += `comment 0 Slot ${i}` + "\n" +

  //                     `comment 0 # <ST 0>            ${NextTime}|${Next}` + "\n" +

  //                     `movie 00:00:00.00 V:\\Content\\${promise1}` + "\n"

  //             }
  //             // str +=  `comment 0 Slot ${i}` + "\n" +
  //             //     `comment 0 # <ST 0>            ${NextTime}|${Next}` + "\n" +
  //             //         `movie 00:00:00.00 V:\\Content\\${promise1}` + "\n"
  //         }
  //     } else {

  //         // console.log('New column name',element["ScrollDetails"])

  //         if (jio.File_Name === '00000') {

  //             str += `comment 0 Slot ${i}` + "\n" +

  //                 `comment 0 # <ST 0>            ${NextTime}|${Next}` + "\n" +

  //                 `video1 00:00:01.00 [0.1]` + "\n"
  //         } else {

  //             if (array[i]["ScrollDetails"] != undefined && array[i]["ScrollDetails"] != null && array[i]["ScrollDetails"] == 'Scroll Off') {

  //                 str += `comment 0 Slot ${i}` + "\n" +

  //                     `comment 0 # <ST 0>            ${NextTime}|${Next}` + "\n" +

  //                     `titleObjAbort {TSF_Ticker_Scroll} 0` + "\n" +
  //                     `movie 00:00:00.00 V:\\Content\\${promise1}` + "\n" +
  //                     `titleObjOn {TSF_Ticker_Scroll} 0`

  //             } else {

  //                 str += `comment 0 Slot ${i}` + "\n" +

  //                     `comment 0 # <ST 0>            ${NextTime}|${Next}` + "\n" +

  //                     `movie 00:00:00.00 V:\\Content\\${promise1}` + "\n"

  //             }
  //             // str +=  `comment 0 Slot ${i}` + "\n" +
  //             //     `comment 0 # <ST 0>            ${NextTime}|${Next}` + "\n" +
  //             //         `movie 00:00:00.00 V:\\Content\\${promise1}` + "\n"
  //         }
  //     }

  //     filenamewithextension = promise1;
  //     var fillers_response = [];

  //     let scteMarkersFillers = scteFillers.map((value) => {
  //         return value.trim()
  //     })
  //     let currentFiller = ""
  //     let fillerListTrim = fillers.map((value) => {
  //         return value.trim()
  //     })
  //     console.log("ALL FILLERS ------", fillerListTrim)
  //     console.log("FILLERS WITH SCTE ------", scteMarkersFillers)
  //     if (fillers !== "-") {
  //         for (let filler of fillers) {

  //             var fillersparams = {
  //                 File_Name: filler.trim()
  //             }
  //             const promise2 = await fillervalue(fillersparams); //important
  //             // console.log(`Fillers ${fillersparams}`,promise2)
  //             var n = promise2.includes("Promo");
  //             if (harekrishnaChannelName === 'Hare Krishna Tv Pravachan') {

  //                 if (promise2 === 'No Fillers') {

  //                     str += `live channel="1" crossfade="0.1"` + "\n"

  //                 } else {
  //                     str += `movie 00:00:00.00 V:\\Pravachan Content\\${promise2}` + "\n"
  //                     fillers_response.push(promise2)
  //                 }

  //             } if (harekrishnaChannelName === 'Hare Krishna Tv') {

  //                 // New Requirement HareKrishna
  //                 // console.log("Fillers type",typeof(fillers))
  //                 // console.log("Fillers list",fillers)
  //                 if (promise2 === 'No Fillers') {

  //                     str += `live channel="1" crossfade="0.1"` + "\n"

  //                 } else if (n === true) {
  //                     // console.log("I am in if >>"+n)
  //                     // console.log("Filler params file name", fillersparams.File_Name)
  //                     if (scteMarkersFillers.length > 0) {
  //                         console.log("Includes", scteMarkersFillers.includes(fillersparams.File_Name), fillersparams.File_Name)
  //                         if (scteMarkersFillers.includes(fillersparams.File_Name)) {
  //                             // check if it is last file
  //                             if (scteMarkersFillers.indexOf(fillersparams.File_Name) == scteMarkersFillers.length - 1) {
  //                                 if (currentFiller != "") {
  //                                     str += `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n"
  //                                     str += `<markstop mark="SCTE35" title="Stop ADS" />` + "\n"
  //                                     fillers_response.push(promise2)
  //                                     currentFiller = ""
  //                                 } else {
  //                                     str += `<markstart mark="SCTE35" markid="0" title="Start ADS" />` + "\n"
  //                                     str += `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n"
  //                                     str += `<markstop mark="SCTE35" title="Stop ADS" />` + "\n"
  //                                     fillers_response.push(promise2)
  //                                     currentFiller = ""
  //                                 }
  //                             } else {

  //                                 if (currentFiller != "") {
  //                                     // let currentFillerIndex = fillers.indexOf(currentFiller)
  //                                     // let iteratorFillerIndex = fillers.indexOf(fillersparams.File_Name)
  //                                     console.log("EMPTY")
  //                                     str += `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n"
  //                                     fillers_response.push(promise2)
  //                                     currentFiller = fillersparams.File_Name
  //                                 } else {
  //                                     currentFiller = fillersparams.File_Name
  //                                     str += `<markstart mark="SCTE35" markid="0" title="Start ADS" />` + "\n"
  //                                     str += `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n"
  //                                     fillers_response.push(promise2)
  //                                 }
  //                             }
  //                         } else {
  //                             if (currentFiller != "") {
  //                                 console.log("Inside stop (1)");
  //                                 let currentFillerIndex = fillerListTrim.indexOf(currentFiller)
  //                                 let iteratorFillerIndex = fillerListTrim.indexOf(fillersparams.File_Name)
  //                                 if (iteratorFillerIndex - currentFillerIndex == 1) {
  //                                     str += `<markstop mark="SCTE35" title="Stop ADS" />` + "\n"
  //                                     str += `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n"
  //                                     fillers_response.push(promise2)
  //                                     currentFiller = ""
  //                                 } else {
  //                                     currentFiller = fillersparams.File_Name
  //                                     str += `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n"
  //                                     fillers_response.push(promise2)
  //                                 }
  //                             } else {
  //                                 str += `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n"
  //                                 fillers_response.push(promise2)
  //                             }
  //                         }
  //                     } else {
  //                         str += `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n"
  //                         fillers_response.push(promise2)
  //                     }
  //                     // str += `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n"
  //                     // fillers_response.push(promise2)

  //                 } else if (n === false) {
  //                     // console.log("i"+i)
  //                     // console.log("I am in if >>"+n)
  //                     if (scteMarkersFillers.length > 0) {
  //                         console.log("Includes (2)", scteMarkersFillers.includes(fillersparams.File_Name), fillersparams.File_Name)
  //                         if (scteMarkersFillers.includes(fillersparams.File_Name)) {
  //                             // check if it is last file
  //                             if (scteMarkersFillers.indexOf(fillersparams.File_Name) == scteMarkersFillers.length - 1) {
  //                                 if (currentFiller != "") {

  //                                     str += `comment 0 # <ST 0>            ${NextTime}|${Next}` + "\n" +

  //                                         `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n"
  //                                     str += `<markstop mark="SCTE35" title="Stop ADS" />` + "\n"
  //                                     fillers_response.push(promise2)
  //                                     currentFiller = ""
  //                                 } else {
  //                                     str += `<markstart mark="SCTE35" markid="0" title="Start ADS" />` + "\n"
  //                                     str += `comment 0 # <ST 0>            ${NextTime}|${Next}` + "\n" +

  //                                         `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n"
  //                                     str += `<markstop mark="SCTE35" title="Stop ADS" />` + "\n"
  //                                     fillers_response.push(promise2)
  //                                     currentFiller = ""
  //                                 }
  //                             } else {
  //                                 if (currentFiller != "") {
  //                                     // let currentFillerIndex = fillers.indexOf(currentFiller)
  //                                     // let iteratorFillerIndex = fillers.indexOf(fillersparams.File_Name)
  //                                     console.log("EMPTY")
  //                                     str += `comment 0 # <ST 0>            ${NextTime}|${Next}` + "\n" +

  //                                         `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n"
  //                                     fillers_response.push(promise2)
  //                                     currentFiller = fillersparams.File_Name
  //                                 } else {
  //                                     currentFiller = fillersparams.File_Name
  //                                     str += `<markstart mark="SCTE35" markid="0" title="Start ADS" />` + "\n"
  //                                     str += `comment 0 # <ST 0>            ${NextTime}|${Next}` + "\n" +

  //                                         `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n"
  //                                     fillers_response.push(promise2)
  //                                 }
  //                             }
  //                         } else {
  //                             if (currentFiller != "") {
  //                                 let currentFillerIndex = fillerListTrim.indexOf(currentFiller)
  //                                 let iteratorFillerIndex = fillerListTrim.indexOf(fillersparams.File_Name)
  //                                 if (iteratorFillerIndex - currentFillerIndex == 1) {
  //                                     console.log("Inside stop (2)");
  //                                     str += `<markstop mark="SCTE35" title="Stop ADS" />` + "\n"
  //                                     str += `comment 0 # <ST 0>            ${NextTime}|${Next}` + "\n" +

  //                                         `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n"
  //                                     fillers_response.push(promise2)
  //                                     currentFiller = ""
  //                                 } else {
  //                                     currentFiller = fillersparams.File_Name
  //                                     str += `comment 0 # <ST 0>            ${NextTime}|${Next}` + "\n" +

  //                                         `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n"
  //                                     fillers_response.push(promise2)
  //                                 }
  //                             } else {
  //                                 str += `comment 0 # <ST 0>            ${NextTime}|${Next}` + "\n" +

  //                                     `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n"
  //                                 fillers_response.push(promise2)
  //                             }
  //                         }
  //                     } else {
  //                         str += `comment 0 # <ST 0>            ${NextTime}|${Next}` + "\n" +

  //                             `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n"
  //                         fillers_response.push(promise2)
  //                     }
  //                     // str += `comment 0 # <ST 0>            ${NextTime}|${Next}` + "\n" +

  //                     //     `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n"
  //                     // fillers_response.push(promise2)
  //                 }

  //                 // --------------------
  //             } else {

  //                 // rest all channels Harekrishna Tv and Harekrishna Tv Music

  //                 if (promise2 === 'No Fillers') {

  //                     str += `live channel="1" crossfade="0.1"` + "\n"

  //                 } else if (n === true) {
  //                     // console.log("I am in if >>"+n)
  //                     str += `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n"
  //                     fillers_response.push(promise2)

  //                 } else if (n === false) {
  //                     // console.log("i"+i)
  //                     // console.log("I am in if >>"+n)
  //                     str += `comment 0 # <ST 0>            ${NextTime}|${Next}` + "\n" +

  //                         `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n"
  //                     fillers_response.push(promise2)
  //                 }
  //             }

  //         }
  //     }
  // }
  // console.log("FINAL DATA TO DISPLAY", str)
  // test -----------------------------------------------------

  // original for loop code ----------------------

  for (let element of array) {
    i++;
    // console.log("increment",i)
    var ProgramName = element["ProgramName"] ? element["ProgramName"] : "-";
    var FillersSerialNumber = element["FillersSerialNumber"]
      ? element["FillersSerialNumber"]
      : "-";
    var ScteFillersSerialNumber = element["ScteOnFillers"]
      ? element["ScteOnFillers"]
      : "-";
    var FileName = element["FileName"] ? element["FileName"] : "-";
    var NextTime = element["EndTime/NextTime"]
      ? element["EndTime/NextTime"]
      : "-";
    var Next = element["Next"] ? element["Next"] : "-";
    var fillers = FillersSerialNumber.split(",")
      ? FillersSerialNumber.split(",")
      : "-";
    var scteFillers =
      ScteFillersSerialNumber.split(",") && ScteFillersSerialNumber != "-"
        ? ScteFillersSerialNumber.split(",")
        : [];
    // console.log("SCTE FILLERS --------", scteFillers);

    var jio = {
      File_Name: FileName.trim(),
    };

    const promise1 = await getfilename(jio); //important

    // if (jio.File_Name === null) {
    //     str += `comment 0 # V:\\Content\\00000.txt.mp4|0:00:0.00|<ST 0> ${ProgramName}` + '\n' + `<live channel="1" crossfade="0.1"`
    // }

    if (harekrishnaChannelName === "Hare Krishna Tv Pravachan") {
      if (jio.File_Name === "00000") {
        str +=
          `comment 0 # V:\\Pravachan Content\\${promise1}|0:00:0.00|<ST 0> ${ProgramName}` +
          "\n" +
          `video1 00:00:01.00 [0.1]` +
          "\n";
      } else {
        str +=
          `comment 0 # V:\\Pravachan Content\\${promise1}|0:00:0.00|<ST 0> ${ProgramName}` +
          "\n" +
          `movie 00:00:00.00 V:\\Pravachan Content\\${promise1}` +
          "\n";
      }
    } else {
      // console.log('New column name',element["ScrollDetails"])

      if (jio.File_Name === "00000") {
        str +=
          `comment 0 Slot ${i}` +
          "\n" +
          `comment 0 # <ST 0>            ${NextTime}|${Next}` +
          "\n" +
          `video1 00:00:01.00 [0.1]` +
          "\n";
      } else {
        if (
          element["ScrollDetails"] != undefined &&
          element["ScrollDetails"] != null &&
          element["ScrollDetails"] == "Scroll Off"
        ) {
          str +=
            `comment 0 Slot ${i}` +
            "\n" +
            `comment 0 # <ST 0>            ${NextTime}|${Next}` +
            "\n" +
            `titleObjAbort {TSF_Ticker_Scroll} 0` +
            "\n" +
            `movie 00:00:00.00 V:\\Content\\${promise1}` +
            "\n" +
            `titleObjOn {TSF_Ticker_Scroll} 0`;
        } else {
          str +=
            `comment 0 Slot ${i}` +
            "\n" +
            `comment 0 # <ST 0>            ${NextTime}|${Next}` +
            "\n" +
            `movie 00:00:00.00 V:\\Content\\${promise1}` +
            "\n";
        }

        // str +=  `comment 0 Slot ${i}` + "\n" +

        //     `comment 0 # <ST 0>            ${NextTime}|${Next}` + "\n" +

        //         `movie 00:00:00.00 V:\\Content\\${promise1}` + "\n"
      }
    }
    filenamewithextension = promise1;
    var fillers_response = [];

    let scteMarkersFillers = scteFillers.map((value) => {
      return value.trim();
    });
    let currentFiller = "";
    let fillerListTrim = fillers.map((value) => {
      return value.trim();
    });
    // console.log("ALL FILLERS ------", fillerListTrim)
    // console.log("FILLERS WITH SCTE ------", scteMarkersFillers)

    if (fillers !== "-") {
      for (let filler of fillers) {
        var fillersparams = {
          File_Name: filler.trim(),
        };
        const promise2 = await fillervalue(fillersparams); //important
        var n = promise2.includes("Promo");
        if (harekrishnaChannelName === "Hare Krishna Tv Pravachan") {
          if (promise2 === "No Fillers") {
            str += `live channel="1" crossfade="0.1"` + "\n";
          } else {
            str +=
              `movie 00:00:00.00 V:\\Pravachan Content\\${promise2}` + "\n";
            fillers_response.push(promise2);
          }
        } else if (harekrishnaChannelName === "Hare Krishna Tv") {
          if (promise2 === "No Fillers") {
            str += `live channel="1" crossfade="0.1"` + "\n";
          } else if (n === true) {
            // console.log("I am in if >>"+n)
            // console.log("Filler params file name", fillersparams.File_Name)
            if (scteMarkersFillers.length > 0) {
              // console.log("Includes", scteMarkersFillers.includes(fillersparams.File_Name), fillersparams.File_Name)
              if (scteMarkersFillers.includes(fillersparams.File_Name)) {
                // check if it is last file
                if (
                  scteMarkersFillers.indexOf(fillersparams.File_Name) ==
                  scteMarkersFillers.length - 1
                ) {
                  if (currentFiller != "") {
                    str += `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n";
                    str += `markstop {SCTE35} 0 Stop ADS` + "\n";
                    fillers_response.push(promise2);
                    currentFiller = "";
                  } else {
                    str += `markstart {SCTE35/0} 0 Start ADS` + "\n";
                    str += `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n";
                    str += `markstop {SCTE35} 0 Stop ADS` + "\n";
                    fillers_response.push(promise2);
                    currentFiller = "";
                  }
                } else {
                  if (currentFiller != "") {
                    // let currentFillerIndex = fillers.indexOf(currentFiller)
                    // let iteratorFillerIndex = fillers.indexOf(fillersparams.File_Name)
                    // console.log("EMPTY")
                    str += `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n";
                    fillers_response.push(promise2);
                    currentFiller = fillersparams.File_Name;
                  } else {
                    currentFiller = fillersparams.File_Name;
                    str += `markstart {SCTE35/0} 0 Start ADS` + "\n";
                    str += `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n";
                    fillers_response.push(promise2);
                  }
                }
              } else {
                if (currentFiller != "") {
                  // console.log("Inside stop (1)");
                  let currentFillerIndex =
                    fillerListTrim.indexOf(currentFiller);
                  let iteratorFillerIndex = fillerListTrim.indexOf(
                    fillersparams.File_Name
                  );
                  if (iteratorFillerIndex - currentFillerIndex == 1) {
                    str += `markstop {SCTE35} 0 Stop ADS` + "\n";
                    str += `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n";
                    fillers_response.push(promise2);
                    currentFiller = "";
                  } else {
                    currentFiller = fillersparams.File_Name;
                    str += `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n";
                    fillers_response.push(promise2);
                  }
                } else {
                  str += `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n";
                  fillers_response.push(promise2);
                }
              }
            } else {
              str += `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n";
              fillers_response.push(promise2);
            }
            // str += `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n"
            // fillers_response.push(promise2)
          } else if (n === false) {
            // console.log("i"+i)
            // console.log("I am in if >>"+n)
            if (scteMarkersFillers.length > 0) {
              // console.log("Includes (2)", scteMarkersFillers.includes(fillersparams.File_Name), fillersparams.File_Name)
              if (scteMarkersFillers.includes(fillersparams.File_Name)) {
                // check if it is last file
                if (
                  scteMarkersFillers.indexOf(fillersparams.File_Name) ==
                  scteMarkersFillers.length - 1
                ) {
                  if (currentFiller != "") {
                    str +=
                      `comment 0 # <ST 0>            ${NextTime}|${Next}` +
                      "\n" +
                      `movie 00:00:00.00 V:\\Content\\${promise2}` +
                      "\n";
                    str += `markstop {SCTE35} 0 Stop ADS` + "\n";
                    fillers_response.push(promise2);
                    currentFiller = "";
                  } else {
                    str += `markstart {SCTE35/0} 0 Start ADS` + "\n";
                    str +=
                      `comment 0 # <ST 0>            ${NextTime}|${Next}` +
                      "\n" +
                      `movie 00:00:00.00 V:\\Content\\${promise2}` +
                      "\n";
                    str += `markstop {SCTE35} 0 Stop ADS` + "\n";
                    fillers_response.push(promise2);
                    currentFiller = "";
                  }
                } else {
                  if (currentFiller != "") {
                    // let currentFillerIndex = fillers.indexOf(currentFiller)
                    // let iteratorFillerIndex = fillers.indexOf(fillersparams.File_Name)
                    // console.log("EMPTY")
                    str +=
                      `comment 0 # <ST 0>            ${NextTime}|${Next}` +
                      "\n" +
                      `movie 00:00:00.00 V:\\Content\\${promise2}` +
                      "\n";
                    fillers_response.push(promise2);
                    currentFiller = fillersparams.File_Name;
                  } else {
                    currentFiller = fillersparams.File_Name;
                    str += `markstart {SCTE35/0} 0 Start ADS` + "\n";
                    str +=
                      `comment 0 # <ST 0>            ${NextTime}|${Next}` +
                      "\n" +
                      `movie 00:00:00.00 V:\\Content\\${promise2}` +
                      "\n";
                    fillers_response.push(promise2);
                  }
                }
              } else {
                if (currentFiller != "") {
                  let currentFillerIndex =
                    fillerListTrim.indexOf(currentFiller);
                  let iteratorFillerIndex = fillerListTrim.indexOf(
                    fillersparams.File_Name
                  );
                  if (iteratorFillerIndex - currentFillerIndex == 1) {
                    // console.log("Inside stop (2)");
                    str += `markstop {SCTE35} 0 Stop ADS` + "\n";
                    str +=
                      `comment 0 # <ST 0>            ${NextTime}|${Next}` +
                      "\n" +
                      `movie 00:00:00.00 V:\\Content\\${promise2}` +
                      "\n";
                    fillers_response.push(promise2);
                    currentFiller = "";
                  } else {
                    currentFiller = fillersparams.File_Name;
                    str +=
                      `comment 0 # <ST 0>            ${NextTime}|${Next}` +
                      "\n" +
                      `movie 00:00:00.00 V:\\Content\\${promise2}` +
                      "\n";
                    fillers_response.push(promise2);
                  }
                } else {
                  str +=
                    `comment 0 # <ST 0>            ${NextTime}|${Next}` +
                    "\n" +
                    `movie 00:00:00.00 V:\\Content\\${promise2}` +
                    "\n";
                  fillers_response.push(promise2);
                }
              }
            } else {
              str +=
                `comment 0 # <ST 0>            ${NextTime}|${Next}` +
                "\n" +
                `movie 00:00:00.00 V:\\Content\\${promise2}` +
                "\n";
              fillers_response.push(promise2);
            }
            // str += `comment 0 # <ST 0>            ${NextTime}|${Next}` + "\n" +

            //     `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n"
            // fillers_response.push(promise2)
          }
        } else {
          if (promise2 === "No Fillers") {
            str += `live channel="1" crossfade="0.1"` + "\n";
          } else if (n === true) {
            // console.log("I am in if >>"+n)
            str += `movie 00:00:00.00 V:\\Content\\${promise2}` + "\n";
            fillers_response.push(promise2);
          } else if (n === false) {
            // console.log("i"+i)
            // console.log("I am in if >>"+n)
            str +=
              `comment 0 # <ST 0>            ${NextTime}|${Next}` +
              "\n" +
              `movie 00:00:00.00 V:\\Content\\${promise2}` +
              "\n";
            fillers_response.push(promise2);
          }
        }
      }
    }
  }

  // console.log("FINAL DATA TO DISPLAY", str)
  var text = str;
  download(harefile, text);
  toastr.success(`${harefile} \n File Has been Downloaded Successfully.`);
}

var userChannelName1 = "";
var file1 = "";
$(document).on("click", ".download-nowFile", function (event) {
  var userName = $(this).attr("userName");
  var File_name = $(this).attr("fileName");

  let params = {
    Username: userName,
    fileName: File_name,
  };

  originalString = File_name;
  newSting2 = originalString.replace(".xlsx", "");
  file1 = newSting2 + ".txt";
  $.post(
    `/v1/fileManagement/getFileDatabyFilenameForOutput`,
    params,
    function (data, status) {
      console.log("data--", data);
      if (data.status == 200) {
        var jsonData = data.data.fileData;
        userChannelName1 = data.data.ChannelName;

        if (data.data.UserName === "onetake") {
          OnetakeNowFileDownload(jsonData);
        } else if (data.data.UserName === "harekrishna") {
          fillersforNowFile(jsonData);
        } else if (data.data.UserName === "pitaara") {
          PitaaraNowFileDownload(jsonData);
        }
      }
    }
  );
});

var popUpUserChannelName = "";
var popUpfile = "";
$(document).on("click", ".download-popFile", function (event) {
  var userName = $(this).attr("userName");
  var File_name = $(this).attr("fileName");

  let params = {
    Username: userName,
    fileName: File_name,
  };

  originalString = File_name;
  newSting2 = originalString.replace(".xlsx", "");
  popUpfile = newSting2 + ".txt";
  $.post(
    `/v1/fileManagement/getFileDatabyFilenameForOutput`,
    params,
    function (data, status) {
      if (data.status == 200) {
        var jsonData = data.data.fileData;
        popUpUserChannelName = data.data.ChannelName;

        if (data.data.UserName === "harekrishna") {
          fillersforPOPFile(jsonData);
        }
      }
    }
  );
});

async function fillersforNowFile(jsonData) {
  var array = jsonData;
  var arraylenght = array.length;
  var harekrishnaChannelName = userChannelName1.trim();

  var options_table = [];
  var str = ``;

  for (let element of array) {
    var ProgramName = element["ProgramName"] ? element["ProgramName"] : "-";
    var FillersSerialNumber = element["FillersSerialNumber"]
      ? element["FillersSerialNumber"]
      : "-";
    var FileName = element["FileName"] ? element["FileName"] : "-";
    var EpisodeTitle = element["EpisodeTitle"] ? element["EpisodeTitle"] : "-";
    var StarCast = element["StarCast"] ? element["StarCast"] : "-";
    var fillers = FillersSerialNumber.split(",")
      ? FillersSerialNumber.split(",")
      : "-";

    var jio = {
      File_Name: FileName.trim(),
    };

    const promise1 = await getfilename(jio);

    if (harekrishnaChannelName === "Hare Krisna Music") {
      if (StarCast === "-" && EpisodeTitle !== "-") {
        str +=
          `V:\\Content\\${promise1}|<ST 0> ${ProgramName} - ${EpisodeTitle}` +
          "\n";
      } else if (EpisodeTitle === "-" && StarCast !== "-") {
        str +=
          `V:\\Content\\${promise1}|<ST 0> ${ProgramName} | ${StarCast}` + "\n";
      } else if (EpisodeTitle === "-" && StarCast === "-") {
        str += `V:\\Content\\${promise1}|<ST 0> ${ProgramName}` + "\n";
      } else {
        str +=
          `V:\\Content\\${promise1}|<ST 0> ${ProgramName} - ${EpisodeTitle} | ${StarCast}` +
          "\n";
      }
    } else if (harekrishnaChannelName === "Hare Krishna Tv") {
      if (ProgramName == "-") {
        str += `V:\\Content\\${promise1}` + "\n";
      } else {
        if (StarCast === "-" && EpisodeTitle !== "-") {
          str +=
            `V:\\Content\\${promise1}|<ST 0> ${ProgramName} - ${EpisodeTitle}` +
            "\n";
        } else if (EpisodeTitle === "-" && StarCast !== "-") {
          str +=
            `V:\\Content\\${promise1}|<ST 0> ${ProgramName} | ${StarCast}` +
            "\n";
        } else if (EpisodeTitle === "-" && StarCast === "-") {
          str += `V:\\Content\\${promise1}|<ST 0> ${ProgramName}` + "\n";
        } else {
          str +=
            `V:\\Content\\${promise1}|<ST 0> ${ProgramName} - ${EpisodeTitle} | ${StarCast}` +
            "\n";
        }
      }
    } else if (harekrishnaChannelName === "Hare Krishna Tv Pravachan") {
      if (StarCast === "-" && EpisodeTitle !== "-") {
        str +=
          `V:\\Pravachan Content\\${promise1}|<ST 0> ${ProgramName} - ${EpisodeTitle}` +
          "\n";
      } else if (EpisodeTitle === "-" && StarCast !== "-") {
        str +=
          `V:\\Pravachan Content\\${promise1}|<ST 0> ${ProgramName} | ${StarCast}` +
          "\n";
      } else if (EpisodeTitle === "-" && StarCast === "-") {
        str +=
          `V:\\Pravachan Content\\${promise1}|<ST 0> ${ProgramName}` + "\n";
      } else {
        str +=
          `V:\\Pravachan Content\\${promise1}|<ST 0> ${ProgramName} - ${EpisodeTitle} | ${StarCast}` +
          "\n";
      }

      //  str += `X:\\HareKrsnaContent\\${promise1}|<ST 0> ${ProgramName} - ${EpisodeTitle} | ${StarCast}` + "\n"
    }
  }

  var text = str;
  downloadNow(file1, text);
  toastr.success(`${file1} \n Now File Has been Downloaded Successfully.`);
}

async function fillersforPOPFile(jsonData) {
  var array = jsonData;

  var arraylenght = array.length;
  var harekrishnaChannelName = popUpUserChannelName;
  var options_table = [];
  var str1 = ``;
  var str2 = ``;
  var str3 = ``;

  for (let element of array) {
    var ProgramName = element["ProgramName"] ? element["ProgramName"] : "-";
    var FillersSerialNumber = element["FillersSerialNumber"]
      ? element["FillersSerialNumber"]
      : "-";
    var FileName = element["FileName"] ? element["FileName"] : "-";
    var EpisodeTitle = element["EpisodeTitle"] ? element["EpisodeTitle"] : "-";
    var StarCast = element["StarCast"] ? element["StarCast"] : "-";
    var POPUPImageVideo_1 = element["POPUPImage/Video_1"]
      ? element["POPUPImage/Video_1"]
      : "";
    var POPUPImageVideo_2 = element["POPUPImage/Video_2"]
      ? element["POPUPImage/Video_2"]
      : "";
    var POPUPImageVideo_3 = element["POPUPImage/Video_3"]
      ? element["POPUPImage/Video_3"]
      : "";
    var fillers = FillersSerialNumber.split(",")
      ? FillersSerialNumber.split(",")
      : "-";

    var jio = {
      File_Name: FileName.trim(),
    };

    const promise1 = await getfilename(jio);

    if (harekrishnaChannelName === "Hare Krisna Music") {
      str1 +=
        `V:\\Content\\${promise1} |V:\\POP Up Videos\\${POPUPImageVideo_1}` +
        "\n";

      str2 +=
        `V:\\Content\\${promise1} |V:\\POP Up Videos\\${POPUPImageVideo_2}` +
        "\n";

      str3 +=
        `V:\\Content\\${promise1} |V:\\POP Up Videos\\${POPUPImageVideo_3}` +
        "\n";
    } else if (harekrishnaChannelName === "Hare Krishna Tv") {
      str1 +=
        `V:\\Content\\${promise1} |V:\\POP Up Videos\\${POPUPImageVideo_1}` +
        "\n";

      str2 +=
        `V:\\Content\\${promise1} |V:\\POP Up Videos\\${POPUPImageVideo_2}` +
        "\n";

      str3 +=
        `V:\\Content\\${promise1} |V:\\POP Up Videos\\${POPUPImageVideo_3}` +
        "\n";
    } else if (harekrishnaChannelName === "Hare Krishna Tv Pravachan") {
      str1 +=
        `V:\\Content\\${promise1} |V:\\POP Up Videos\\${POPUPImageVideo_1}` +
        "\n";

      str2 +=
        `V:\\Content\\${promise1} |V:\\POP Up Videos\\${POPUPImageVideo_2}` +
        "\n";

      str3 +=
        `V:\\Content\\${promise1} |V:\\POP Up Videos\\${POPUPImageVideo_3}` +
        "\n";
    }
  }

  var text1 = str1;
  var text2 = str2;
  var text3 = str3;

  downloadNow("POP Up 1 " + popUpfile, text1);
  downloadNow("POP Up 2 " + popUpfile, text2);
  downloadNow("POP Up 3 " + popUpfile, text3);
  toastr.success(`${popUpfile} \n Now File Has been Downloaded Successfully.`);
}

function OnetakeNowFileDownload(jsonData) {
  var array = jsonData;
  var channelName = userChannelName1.trim();
  var options_table = "";
  array.forEach(function (element, i) {
    var ContentID = element.ContentID ? element.ContentID : "-";
    var ProgramName = element.ProgramName ? element.ProgramName : "-";
    var ProgramFile = element.ProgramFile ? element.ProgramFile : "-";
    var Type = element.Type ? element.Type : "-";
    var SCTE = element.SCTE ? element.SCTE : "-";
    var PartNo = element.PartNo ? element.PartNo : "-";

    if (channelName === "Kids Toons") {
      if (ProgramName !== "-") {
        options_table +=
          `X:\\kids toons__new\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        options_table +=
          `X:\\kids toons__new\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "Kids Toons Punjabi") {
      if (ProgramName !== "-") {
        options_table +=
          `X:\\KIDS_Toon_Punjabi\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        options_table +=
          `X:\\KIDS_Toon_Punjabi\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "Cooking") {
      if (ProgramName !== "-") {
        options_table +=
          `X:\\COOKING_NEW\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        options_table += `X:\\COOKING_NEW\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "Kids Rhymes") {
      if (ProgramName !== "-") {
        options_table +=
          `X:\\KIDS RHYMES_NEW\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        options_table +=
          `X:\\KIDS RHYMES_NEW\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "Hollywood Action") {
      if (ProgramName !== "-") {
        options_table +=
          `X:\\HOLLYWOOD ACTION_NEW\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        options_table +=
          `X:\\HOLLYWOOD ACTION_NEW\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "Bhojpuri Songs") {
      if (ProgramName !== "-") {
        options_table +=
          `Y:\\BHOJPURI_NEW\\SONGS\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        options_table +=
          `Y:\\BHOJPURI_NEW\\SONGS\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "Bengali Movies") {
      if (ProgramName !== "-") {
        options_table +=
          `X:\\BENGALI_NEW\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        options_table += `X:\\BENGALI_NEW\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "Bhojpuri Movies") {
      if (ProgramName !== "-") {
        options_table +=
          `X:\\BHOJPURI_NEW\\${Type}\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        options_table +=
          `X:\\BHOJPURI_NEW\\${Type}\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "Hollywood Gujrati Movies") {
      if (ProgramName !== "-") {
        options_table +=
          `S:\\GUJARATI MOVIES_NEW\\Movies\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        options_table +=
          `S:\\GUJARATI MOVIES_NEW\\Movies\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "Hollywood Marathi Movies") {
      if (ProgramName !== "-") {
        options_table +=
          `S:\\MARATHI MOVIE_NEW\\Movies\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        options_table +=
          `S:\\MARATHI MOVIE_NEW\\Movies\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "Hollywood Movies") {
      if (ProgramName !== "-") {
        options_table +=
          `Y:\\KCCL_HOLLYWOOD ONE_NEW\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        options_table +=
          `Y:\\KCCL_HOLLYWOOD ONE_NEW\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "Hollywood Tamil") {
      if (ProgramName !== "-") {
        options_table +=
          `Y:\\TAMIL_NEW\\MOVIES\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        options_table +=
          `Y:\\TAMIL_NEW\\MOVIES\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "Hollywood Telugu") {
      if (ProgramName !== "-") {
        options_table +=
          `Y:\\TELUGU_NEW\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        options_table += `Y:\\TELUGU_NEW\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "K-PoP") {
      if (ProgramName !== "-") {
        options_table +=
          `Y:\\KOREAN_NEW\\K-POP\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        options_table +=
          `Y:\\KOREAN_NEW\\K-POP\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "K-World") {
      if (ProgramName !== "-") {
        options_table +=
          `X:\\KOREAN_NEW\\Content\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        options_table +=
          `X:\\KOREAN_NEW\\Content\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "Kids Movies") {
      if (ProgramName !== "-") {
        options_table +=
          `X:\\KIDS MOVIES_NEW\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        options_table +=
          `X:\\KIDS MOVIES_NEW\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "Kids Toons Marathi") {
      if (ProgramName !== "-") {
        options_table +=
          `S:\\KIDS TOONS MARATHI_NEW\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        options_table +=
          `S:\\KIDS TOONS MARATHI_NEW\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "Malayalam") {
      if (ProgramName !== "-") {
        options_table +=
          `X:\\MALYALAM_NEW\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        options_table += `X:\\MALYALAM_NEW\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "South Action") {
      if (ProgramName !== "-") {
        options_table +=
          `X:\\SOUTH ACTION_NEW\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        options_table +=
          `X:\\SOUTH ACTION_NEW\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "Hollywood Action Punjabi") {
      if (ProgramName !== "-") {
        options_table +=
          `X:\\HOLLYWOOD ACTION PUNJABI\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        options_table +=
          `X:\\HOLLYWOOD ACTION PUNJABI\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "Hollywood Action Bangla") {
      if (ProgramName !== "-") {
        options_table +=
          `X:\\HOLLYWOOD ACTION BANGLA\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        options_table +=
          `X:\\HOLLYWOOD ACTION BANGLA\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "Cooking SCTE") {
      if (ProgramName !== "-" && SCTE == "Start ADS") {
        options_table +=
          `X:\\COOKING_NEW\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        // options_table += `X:\\COOKING_NEW\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "Hollywood Action SCTE") {
      if (ProgramName !== "-" && SCTE == "Start ADS") {
        options_table +=
          `X:\\HOLLYWOOD ACTION_NEW\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        // options_table += `X:\\HOLLYWOOD ACTION_NEW\\${ContentID}_${ProgramFile}` + "\n";
      }
      // } else if (channelName === 'Kids Toons SCTE') {
    } else if (channelName === "Comedy King SCTE") {
      if (ProgramName !== "-" && SCTE == "Start ADS") {
        options_table +=
          `X:\\COMEDY KING\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        // options_table += `X:\\KIDS TOONS_NEW\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "K-World SCTE") {
      if (ProgramName !== "-" && SCTE == "Start ADS") {
        options_table +=
          `X:\\K-Word Fast TV\\${Type}\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        // options_table += `X:\\KOREAN_NEW\\${Type}\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "Hollywood Desi SCTE") {
      if (ProgramName !== "-" && SCTE == "Start ADS") {
        options_table +=
          `X:\\HOLLYWOOD ACTION_NEW\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        // options_table += `X:\\HOLLYWOOD ACTION_NEW\\${ContentID}_${ProgramFile}` + "\n";
      }

      // } else if (channelName === 'Hollywood Movies SCTE') {
    } else if (channelName === "Bhakti Play SCTE") {
      if (ProgramName !== "-" && PartNo !== "-") {
        options_table +=
          `Y:\\KCCL_HOLLYWOOD ONE_NEW\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        // options_table += `Y:\\KCCL_HOLLYWOOD ONE_NEW\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "Hooray Rhymes SCTE") {
      if (ProgramName !== "-" && SCTE == "Start ADS") {
        options_table +=
          `X:\\HOORAY RHYMES\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        // options_table += `X:\\HOORAY RHYMES\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "Hooray Kids SCTE") {
      if (ProgramName !== "-" && SCTE == "Start ADS") {
        options_table +=
          `X:\\HOORAY_KIDS_NEW\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        // options_table += `X:\\HOORAY RHYMES\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "Kiddo Matix SCTE") {
      if (ProgramName !== "-" && SCTE == "Start ADS") {
        options_table +=
          `X:\\Kiddo_Matrix_New\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        // options_table += `X:\\HOORAY RHYMES\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "Comedy Club") {
      if (ProgramName !== "-") {
        options_table +=
          `X:\\COMEDY KING\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        options_table += `X:\\COMEDY KING\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "Bhakti Play") {
      if (ProgramName !== "-") {
        options_table +=
          `X:\\BHAKTI PLAY\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        options_table += `X:\\BHAKTI PLAY\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "K-World(Telugu)") {
      if (ProgramName !== "-") {
        options_table +=
          `X:\\KOREAN_TELUGU_NEW\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        options_table +=
          `X:\\KOREAN_TELUGU_NEW\\${ContentID}_${ProgramFile}` + "\n";
      }
    } else if (channelName === "K-World(Tamil)") {
      if (ProgramName !== "-") {
        options_table +=
          `X:\\KOREAN_TAMIL_NEW\\${ContentID}_${ProgramFile}|<ST 0>${ProgramName}` +
          "\n";
      } else {
        options_table +=
          `X:\\KOREAN_TAMIL_NEW\\${ContentID}_${ProgramFile}` + "\n";
      }
    }

    if (i == array.length - 1) {
      //initiate for 1st row
      var filedata = options_table;
      var text = filedata;
      downloadNow(file1, text);
      toastr.success(`${file1} \n File Has been Downloaded Successfully.`);
    }
  });
}

function PitaaraNowFileDownload(jsonData) {
  var array = jsonData;
  var channelName = userChannelName1.trim();
  var options_table = "";
  const regexFilter = new RegExp(/^.{3}(MOV)/);
  const regexFilterUk = new RegExp(/^.{6}(MOV)/);

  console.log("Channel Name", channelName);

  const SELECTED_CATEGORY = [
    "PROG",
    "PROG9",
    "PROG1",
    "PROGWTP1",
    "PROGWTP2",
    "PROGWTP9",
  ];

  if (channelName === "Pitaara India") {
    array.forEach(function (element, i) {
      var Category = element.Category ? element.Category : "-";
      var ProgramName = element.Title ? element.Title : "-";
      var Location = element.Location ? element.Location : "-";
      var FPC_PROGRAMME_NAME = element.FPC_PROGRAMME_NAME
        ? element.FPC_PROGRAMME_NAME
        : "-";
      let locationWithPathChange = "";
      if (Location !== "-") {
        locationWithPathChange = Location.replace(Location[0], "V");
      } else {
        locationWithPathChange = Location;
      }

      // next time use
      // if ((Category == "PROG1") || (Category == "PROG")) {
      if (SELECTED_CATEGORY.includes(Category)) {
        options_table +=
          `${locationWithPathChange}|<ST 0>|${ProgramName}` + "\n";
      }
      // var ContentID = element.ContentID ? element.ContentID : "-";
      // var ProgramName = element.ProgramName ? element.ProgramName : "-";
      // var ProgramFile = element.ProgramFile ? element.ProgramFile : "-";
      // var Type = element.Type ? element.Type : "-";

      // console.log("File Main",element)

      if (i == array.length - 1) {
        //initiate for 1st row
        var filedata = options_table;
        var text = filedata;
        downloadNow(file1, text);
        toastr.success(`${file1} \n File Has been Downloaded Successfully.`);
        filedata = "";
      }
    });
  } else if (channelName === "Pitaara UK") {
    array.forEach(function (element, i) {
      var Category = element.Category ? element.Category : "-";
      var ProgramName = element.Title ? element.Title : "-";
      var FPC_PROGRAMME_NAME = element.FPC_PROGRAMME_NAME
        ? element.FPC_PROGRAMME_NAME
        : "-";
      var ProgramName = element.Title ? element.Title : "-";
      var Location = element.Location ? element.Location : "-";
      let locationWithPathChange = "";
      if (Location !== "-") {
        locationWithPathChange = Location.replace(Location[0], "V");
      } else {
        locationWithPathChange = Location;
      }

      // if ((Category == "PROG1") || (Category == "PROG")) {
      if (SELECTED_CATEGORY.includes(Category)) {
        options_table +=
          `${locationWithPathChange}|<ST 0>|${ProgramName}` + "\n";
      }
      // var ContentID = element.ContentID ? element.ContentID : "-";
      // var ProgramName = element.ProgramName ? element.ProgramName : "-";
      // var ProgramFile = element.ProgramFile ? element.ProgramFile : "-";
      // var Type = element.Type ? element.Type : "-";

      // console.log("File Main",element)

      if (i == array.length - 1) {
        //initiate for 1st row
        var filedata = options_table;
        var text = filedata;
        downloadNow(file1, text);
        toastr.success(`${file1} \n File Has been Downloaded Successfully.`);
        filedata = "";
      }
    });
  }
}
