var query = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
  };
  var fileName = query('fileName') ? query('fileName') : null;


// alert("fileName"+fileName)

  $.get(`/v1/fileManagement/getFileDatabyEpgFilenameforDisplay?fileName=${fileName}`,
function (data, status) {
    console.log("Data to be displayed",data)
    if (data.status === 200) {
        
       //console.log("data EPG"+JSON.stringify(data.data))
        if (data.data === null) {
            var data1 = data.data
            Swal.fire({
                title: 'No EPG file Available for this Channel',
                showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                },
                hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                }
            })

            $('#sample_div').addClass("display-hidden")
            $('#sample_div1').addClass("display-hidden")
            $('#sample_divOntake').addClass("display-hidden")
        } else {
            var ClientName = data.data.UserName
            var GetfileDataEpg = data.data
           

            if (ClientName === 'onetake') {
                appendFiledataOnetakeEpgfileName(GetfileDataEpg)
            } else if(ClientName === 'wellness'){
                appendFiledatabyEpgfileName(GetfileDataEpg)
            } else if (ClientName === 'srmd'){
            appendFiledataSrmdEpgfileName(GetfileDataEpg)  
             }
        }

    } else {
        console.log("get getFileDatabyFilename Failed")
    }
})




function appendFiledataOnetakeEpgfileName(GetfileDataEpg) {
    if (GetfileDataEpg !== null) {
        var array = GetfileDataEpg.fileData;
      //  console.log(JSON.stringify(array))

        var options_table = "";

        array.forEach(function (element, i) {

            var Date = element.Date ? element.Date : "-";
            var Time = element.Time ? element.Time : "-";
            var Duration = element.Duration ? element.Duration : "-";
            var Title = element.Title ? element.Title : "-";
            var Synopsis = element.Synopsis ? element.Synopsis : "-";
            var Genure = element.Genure ? element.Genure : "-";
            var Sub_Genure = element.SubGenure ? element.SubGenure : "-";
            var Language = element.Language ? element.Language : "-";


            options_table += `<tr class="users-tbl-row asset-row">
                   <td class="date">${Date}</td>
                   <td class="name">${Time}</td>
                   <td class="name">${Duration}</td>
                   <td class="name">${Title}</td>
                   <td class="name" >${Synopsis}</td>
                   <td class="name">${Genure}</td>
                   <td class="text-break">${Sub_Genure}</td>
                   <td class="name">${Language}</td>`;

            if (i == array.length - 1) {
                //initiate for 1st row
                $('#epgOnetakedata_tbody').append(options_table);

            }
        })

        $('#sample_div').addClass("display-hidden")
        $('#sample_div1').addClass("display-hidden")
    }

}



function appendFiledataSrmdEpgfileName(GetfileDataEpg) {
    if (GetfileDataEpg !== null) {
        
        var array = GetfileDataEpg.fileData;
        var options_table = "";

        array.forEach(function (element, i) {
          
            var Date = element.Date ? element.Date : "-";
            var Start_Time = element.StartTime ? element.StartTime : "-";
            var Stop_Time = element.StopTime ? element.StopTime : "-";
            var DuraTion = element.Duration ? element.Duration : "-";
            var Title = element.Title ? element.Title : "-";
            var DescriPtion = element.Description ? element.Description : "-";
            var Is_repeat = element.Isrepeat ? element.Isrepeat : "-";
            var Thumbnail = element.Thumbnail ? element.Thumbnail : "-";
            var Program_Number = element.ProgramNumber ? element.ProgramNumber : "-";

            options_table += `<tr class="users-tbl-row asset-row">
                       <td class="date">${Date}</td>
                       <td class="name">${Start_Time}</td>
                       <td class="name">${Stop_Time}</td>
                       <td class="name">${DuraTion}</td>
                       <td class="name">${Title}</td>
                       <td class="name" >${DescriPtion}</td>
                       <td class="name">${Is_repeat}</td>
                       <td class="text-break">${Thumbnail}</td>
                       <td class="name">${Program_Number}</td>`;

            if (i == array.length - 1) {
                //initiate for 1st row
                $('#epgSrmddata_tbody').append(options_table);
             
            }
        })

 $('#sample_div1').addClass("display-hidden")
 $('#sample_divOntake').addClass("display-hidden")
    }
   
}



function appendFiledatabyEpgfileName(GetfileDataEpg) {
    if (GetfileDataEpg !== null) {

        var array = GetfileDataEpg.fileData
        var options_table = "";

        array.forEach(function (element, i) {

            var Date = element.Date ? element.Date : "-";
            var Start_Time = element.StartTime ? element.StartTime : "-";
            var Stop_Time = element.StopTime ? element.StopTime: "-";
            var Duration = element.Duration ? element.Duration : "-";
            var Title = element.Title ? element.Title : "-";
            var Description = element.Description ? element.Description : "-";
            var Isrepeat = element.Isrepeat ? element.Isrepeat : "-";
            var Thumbnail = element.Thumbnail ? element.Thumbnail : "-";
            var Program_Number = element.ProgramNumber ? element.ProgramNumber : "-";


            options_table += `<tr class="users-tbl-row asset-row">
                   <td class="index">${(i + 1)}</td>
                   <td class="name">${Date}</td>
                   <td class="name">${Start_Time}</td>
                   <td class="name">${Stop_Time}</td>
                   <td class="name">${Duration}</td>
                   <td class="name">${Title}</td>
                   <td class="name">${Description}</td>
                   <td class="name">${Isrepeat}</td>
                   <td class="name">${Thumbnail}</td>
                   <td class="name">${Program_Number}</td>`;

            if (i == array.length - 1) {
                //initiate for 1st row
                $('#epgdata_tbody').append(options_table);

            }
        })


        $('#sample_div').addClass("display-hidden")
        $('#sample_divOntake').addClass("display-hidden")


    }

}