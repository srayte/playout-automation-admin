
var query = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};
var fileName = query('fileName') ? query('fileName') : null;

$.get(`/v1/fileManagement/getFileDatabyFilename?fileName=${fileName}`,
    function (data, status) {
        if (data.status === 200) {
           if (data.data.UserName === 'srmd'){
            var GetSrmdfileData = data.data.fileData
            appendFiledatabyfileNamewithSrmd(GetSrmdfileData)  
           }else if(data.data.UserName === 'wellness') { 
            var GetfileData = data.data.fileData
            appendFiledatabyfileName(GetfileData)
           }
        
        } else {
            console.log("get getFileDatabyFilename Failed")
        }
    })


    function appendFiledatabyfileNamewithSrmd(GetSrmdfileData) {
        var array = GetSrmdfileData.toString()

        var xyz= JSON.parse(array)
       var options_table = "";

       xyz.forEach(function (element, i) {
   
           var Channel = element.Channel ? element.Channel : "-";
           var DATE = element.DATE.toString()
           var year = DATE.substring(0, 4);
           var month = DATE.substring(4, 6);
           var day = DATE.substring(6, 8); 
         
           var  displayDate = year + '-' + month + '-' + day;
           var Event_ID = element.Event_ID ? element.Event_ID : "-";
           var TITLE = element.TITLE ? element.TITLE : "-";
           var DURATION = element.DURATION ? element.DURATION : "-";

         options_table += `<tr class="users-tbl-row asset-row">
              <td class="index">${i + 1}</td>
               <td class="Channel">${Channel}</td>
               <td class="name" style="width:10%">${displayDate}</td>
               <td class="name">${Event_ID}</td>
               <td class="name">${TITLE}</td>
               <td class="name">${DURATION}</td>`;
        
   
          if (i == xyz.length - 1) {
   
           //initiate for 1st row
           $('#srmdxldata_tbody').append(options_table);
       }
       })

       $('#sample_div1').addClass("display-hidden")
    }


function appendFiledatabyfileName(GetfileData) {
    var array = GetfileData.toString()

    var xyz= JSON.parse(array)   
    var options_table = "";
       
    xyz.forEach(function (element, i) { 
    var SrNo = element.SrNo ? element.SrNo : "-";
    var ProgrammeName = element.ProgrammeName ? element.ProgrammeName : "-";
    var TOPIC = element.TOPIC ? element.TOPIC : "-";
    var Subject = element.Subject ? element.Subject : "-";
    var CodeNo = element.CodeNo ? element.CodeNo : "-";
    var Duration = element.Duration ? element.Duration : "-";
   
         options_table += `<tr class="users-tbl-row asset-row">
               <td class="${(i+1)}">${SrNo}</td>
               <td class="name">${ProgrammeName}</td>
               <td class="name">${TOPIC}</td>
               <td class="name">${Subject}</td>
               <td class="name">${CodeNo}</td>
               <td class="name">${Duration}</td>`;
           
          if (i == xyz.length - 1) {
           //initiate for 1st row
           $('#xldata_tbody').append(options_table);
         
       }
       })
       $('#sample_div').addClass("display-hidden")
    }



$(document).ready(function () {

    // Register
    $("#register-form").submit(function (event) {
        $.post('/register',
            {
                name: $('#fullName').val().trim(),
                email: $('#email').val().trim().toLowerCase(),
                password: $("#password").val().trim()
            }

            ,
            function (data, status) {
                if (data.status === 200) {
                    user = data.data.user;
                    toastr.success('User successfully registered.');
                    $('#userRegisterModal').modal('hide');

                    setTimeout(function () {
                        window.location.href = '/v1/clients'
                    }, 3000)

                } else {
                    toastr.danger('User registration failed.')
                }

            });
        event.preventDefault()
    })


    // Login
    $("#login-form").submit(function (event) {
        $.post('/login',
            {
               
                // tenantId: $("#tenantId").val().trim(),
                username: $("#email").val().trim().toLowerCase(),
                password: $("#password").val().trim()
            },
            function (data, status) {
               

               
                if (data.status === 200 && data.data.idToken.payload['cognito:groups'][0] === 'Admin' ) {
                   
               
                   window.location.href = '/v1/clients';
                  
                } else {
                    toastr.error(data.message);
                }
            });


        event.preventDefault()
    });


    // getUsers
    $.get('/getUsers', function (data, status) {
        if (data.status == 200) {
            destroyRows();
            appendUsersDatatable(data);
        }
    });



    function destroyRows() {
        $('#list_all_users_tbody').empty()
        $('#list_all_user_table').DataTable().rows().remove();
        $("#list_all_user_table").DataTable().destroy()
    }


    function appendUsersDatatable(data) {
        var array = data.data.Users;
        if (array.length) {
            var options_table = "";
            var string2 = "";
            array.forEach(function (element, i) {

                var Username = element.Username ? element.Username : "";
                var Attributes = element.Attributes ? element.Attributes : "";
                var UserCreateDate = element.UserCreateDate ? moment(element.UserCreateDate).format('lll') : "";
                var UserLastModifiedDate = element.UserLastModifiedDate ? moment(element.UserLastModifiedDate).format('lll') : "";
                var Enabled = element.Enabled ? element.Enabled : false;
                var UserStatus = element.UserStatus ? element.UserStatus : "UNCONFIRMED";
                var name = '';
                var string = '';


                if (UserStatus === 'CONFIRMED') {
                    UserStatus = '<span class="badge badge-success">Active</span>'
                } else {
                    UserStatus = '<span class="badge badge-purple">Pending Activation</span>'
                }

                if (Attributes.length) {
                    for (var j = 0; j < Attributes.length; j++) {
                        if (Attributes[j].Name === 'name') {
                            name = Attributes[j].Value;
                        }
                    }
                }

                string += `<option class="user-name pro-user-role" value='${name}' userName='${Username}' >${name}</option>`

                string2 += `<option class="user-name" id="getuserfiledata" value='${Username}' userName='${Username}' >${Username}</option>`

                options_table += `<tr class="users-tbl-row asset-row" id="${Username}">
                <td class="username">${Username}</td>
                <td class="name">${name}</td>
                <td class="state">${UserStatus}</td>
                <td class="action-td" id=${Username}><div class="dropdown"> <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false"> <i class="fe-settings noti-icon"></i> </a> <div class="dropdown-menu dropdown-menu-right"><a href="#" class="dropdown-item delete-user">Delete</a> </div> </div></td>`;

                if (i == array.length - 1) {

                    //initiate for 1st row
                    getUserInfo(array[0].Username)
                    $('#list_all_users_tbody').append(options_table);
                    $('#userName').append(string);
                    $('#Customer_Name').append(string2);
                    reInitializeDataTable()
                }
            })
        } else {
            $('#list_all_users_tbody').append(`<div><h6 class="text-primary">No users found.</h6></div>`);
            $('.user-overview-container').html(`<div class="row"> <div class="col-md-6"> </div> <div class="col-md-6 </div> </div><hr>`)
        }

    }

    function reInitializeDataTable() {
        $("#list_all_user_table").DataTable().destroy()
        list_all_user_table = $('#list_all_user_table').DataTable({
            //"order": [[1, "desc"]], // for descending order
            "columnDefs": [
                { "width": "35%", "targets": 0 },
                { "width": "30%", "targets": 1 }
            ]
        })

        $("#list_all_user_table tbody tr:first").addClass("active");
    }


   

    // authenticateUser
    $("#authentication-form").submit(function (event) {

        $.post('/authenticateUser',
            {

                username: $("#username").val().trim(),
                email: $("#email").val().trim().toLowerCase(),
                password: $("#password").val().trim(),
                newPassword: $("#newPassword").val().trim()
            },
            function (data, status) {
                if (data.status === 200) {
                    toastr.success('Password reset successfully. Sign in to continue.');

                    setTimeout(function () {

                        window.location.href = 'http://air.skandha.tv/user-Authentication';
                    }, 3000)
                } else {
                    toastr.error(data.message);
                }
            });
        event.preventDefault()
    });


    // Delete User
    $(document).on("click", ".delete-user", function () {

        var params = {
            username: $(this).closest(".action-td").attr('id'),
        }

        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            confirmButtonClass: "btn btn-success mt-2",
            cancelButtonClass: "btn btn-danger ml-2 mt-2",
            buttonsStyling: !1
        }).then(function (t) {
            if (t.value) {
                $.post('/deleteUser',
                    params,
                    function (data, status) {
                        if (data.status === 200) {

                            toastr.success('User Deleted');

                            setTimeout(function () {
                                window.location.href = '/v1/clients';
                            }, 3000)

                        } else if (data.status === 401) {
                            toastr.error('Delete User Failed');
                        }
                    });
            }

        })

    })


})


// User row selection
$(document).on("click", ".users-tbl-row", function () {

    $('.users-tbl-row').removeClass("active");
    $(this).addClass("active");

    var username = $(this).attr('id');
    getUserInfo(username);

})




// formatUserInfo
function formatUserInfo(userInfo) {
    var Username = userInfo.Username;
    var UserCreateDate = userInfo.UserCreateDate;
    var UserStatus = userInfo.UserStatus;
    var enabled = userInfo.Enabled;
    var enable_btn;
    var email_verified;
    var name;

    if (UserStatus === 'CONFIRMED') {
        UserStatus = `<span class="badge badge-success">Active</span>`
    } else {
        UserStatus = `<span class="badge badge-purple">Pending Activation</span>`
    }

    if (enabled) {
        enabled = `<i class="mdi mdi-account-check"></i><span class="text-success">Enabled</span>`;
        enable_btn = `<button class="btn btn-xs btn-icon waves-effect waves-light btn-danger" title="Disable User" id="disable-user-btn"><i class="mdi mdi-account-off"></i> </button>`
    } else {
        enabled = `<i class="mdi mdi-account-off"></i><span class="text-danger">Disabled</span>`;
        enable_btn = `<button class="btn btn-xs btn-icon waves-effect waves-light btn-success" title="Enable User" id="enable-user-btn"><i class="mdi mdi-account-check"></i> </button>`
    }

    var userAttributesArr = userInfo.UserAttributes;
    for (var i = 0; i < userAttributesArr.length; i++) {

        if (userAttributesArr[i].Name === "email_verified") {
            email_verified = (userAttributesArr[i].Value === "true") ? true : false;
            if (email_verified) {
                email_verified = `<i class="mdi mdi-shield-check"></i><span class="text-success">Verified</span>`
            } else {
                email_verified = `<i class="mdi mdi-shield-lock"></i><span class="text-warning">Pending</span>`
            }
        } else {
            email_verified = `<i class="mdi mdi-shield-check"></i><span class="text-success">Verified</span>`
        }

        if (userAttributesArr[i].Name === "name") {
            name = userAttributesArr[i].Value;
        }

    }

    var options = ``
    options = `<div class="row">

    <div class="col-md-6"> 
     </div>
    <div class="col-md-6 text-right action-btns">${enable_btn}</div>
    </div><hr>
    <div class="row" id="user-info-container">
    <div class="col-md-12"><h4 class="info-username" id="info_username" data-username="${Username}">${name}</h4></div>
    <div class="col-md-4"><h5 class="info-role text-muted">Role : </h5></div><div class="col-md-8" style="padding-top: 8px;">User</div>
    <div class="col-md-4"><h5 class="info-joined text-muted">Joined : </h5></div><div class="col-md-8" style="padding-top: 8px;">${moment(UserCreateDate).format('lll')}</div>
    <div class="col-md-4"><h5 class="info-email text-muted">Email : </h5></div><div class="col-md-8" style="padding-top: 8px;">${email_verified}</div>
    <div class="col-md-4"><h5 class="info-enabled text-muted">Enabled : </h5></div><div class="col-md-8" style="padding-top: 8px;">${enabled}</div>
    <div class="col-md-6"> </div>
    </div>`


    $('.user-overview-container').html(options)

}


// getUserInfo
function getUserInfo(username) {
    $.get(`/getUserInfo/${username}`,
        function (data, status) {
            if (data.status == 200) {
                //console.log("getUserInfo >> : " + JSON.stringify(data))
                formatUserInfo(data.data)
            }
        });
}


// Enable User
$(document).on("click", "#enable-user-btn", function () {

    let params = {
        username: $('#info_username').attr('data-username')
    }

    $.post(`/v1/users/enableUser`,
        params,
        function (data, status) {
            if (data.status === 200) {

                toastr.success('User Enabled');

                setTimeout(function () {
                    window.location.href = '/v1/clients';
                }, 3000)

            } else {
                toastr.error('User Disable Failed');
            }
        });

})

// Disable User
$(document).on("click", "#disable-user-btn", function () {

    let params = {
        username: $('#info_username').attr('data-username')
    }

    $.post(`/v1/users/disableUser`,
        params,
        function (data, status) {
            if (data.status === 200) {

                toastr.success('User Disabled');

                setTimeout(function () {
                    window.location.href = '/v1/clients';
                }, 3000)

            } else {
                toastr.error('User Disable Failed');
            }
        });

})











