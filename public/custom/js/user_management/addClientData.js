
var query = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};
var fileName = query('fileName') ? query('fileName') : null;

$.get(`/v1/clientUploadData/getFileDatabyFilename?fileName=${fileName}`,
    function (data, status) {
        if (data.status === 200) {
            if (data === null) {
                var _data = data.data;
            } else {
                var GetHarefileData = data.data;
                appendFiledatabyfileNamewithHare(GetHarefileData)
            }
        } else {
            console.log("get getFileDatabyFilename Failed")
        }
    })


function appendFiledatabyfileNamewithHare(GetHarefileData) {
    if (GetHarefileData !== null) {

        var array = GetHarefileData.fileData;
        var options_table = "";

        array.forEach(function (element, i) {

            var start_time = element["AiringStartTime"] ? element["AiringStartTime"] : "-";
            var end_time = element["EndTime"] ? element["EndTime"] : "-";
            var ProgramName = element["ProgramName"] ? element["ProgramName"] : "-";
            var FileName = element["FileName"] ? element["FileName"] : "-";
            var FillersSerialNumber = element["FillersSerialNumber"] ? element["FillersSerialNumber"] : "-"

            options_table += `<tr class="users-tbl-row asset-row">
                        <td class="#">${i + 1}</td>
                           <td class="Channel" style="width:10%">${start_time}</td>
                           <td class="name">${end_time}</td>
                           <td class="name" style="width:20%">${ProgramName}</td>
                           <td class="name" style="width:29%">${FileName}</td>
                           <td class="name">${FillersSerialNumber}</td>`;
            if (i == array.length - 1) {
                //initiate for 1st row
                $('#harexldata_tbody').append(options_table);
            }
        })
    }

}


$(document).ready(function () {

   

    $("#uploadFormHare").submit(function (event) {
        var formData = new FormData();
        formData.append('userHarekrishna', $('#userHarekrishna')[0].files[0]);
       
        $.ajax({

            url: '/v1/clientUploadData/fileHare',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            async: false,
            processData: false
        }).then(function (data) {
         //  alert("data"+JSON.stringify(data))
            if (data.status === 200 || data) {

                var fileUpload1 = document.getElementById("userHarekrishna");
               
                
                //Validate whether File is valid Excel file.
                if (fileUpload1.value.toLowerCase()) {
                    if (typeof (FileReader) != "undefined") {
                        var reader = new FileReader();

                        //For Browsers other than IE.
                        if (reader.readAsBinaryString) {
                            reader.onload = function (e) {
                                ProcessExcel(e.target.result);
                            };
                            reader.readAsBinaryString(fileUpload1.files[0]);
                        } else {
                            //For IE Browser.
                            reader.onload = function (e) {
                                var data = "";
                                var bytes = new Uint8Array(e.target.result);
                                for (var i = 0; i < bytes.byteLength; i++) {
                                    data += String.fromCharCode(bytes[i]);
                                }

                            
                                ProcessExcel(data);
                            };
                            reader.readAsArrayBuffer(fileUpload1.files[0]);
                        }
                    } else {
                        alert("This browser does not support HTML5.");
                    }
                } else {
                    alert("Please upload a valid Excel file.");
                }
            } else {
                console.log("Oops! JSON Upload ERROR >  " + data.message)
            }
        })
        event.preventDefault()
    })


    function ProcessExcel(data) {

        //Read the Excel File data.
        var workbook = XLSX.read(data, {
            type: 'binary'
        });

        //Fetch the name of First Sheet.
        var firstSheet = workbook.SheetNames[0];

        //Read all rows from First Sheet into an JSON array.
        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);
        
        var params = {
            fileData: excelRows
        }
        $.post('/v1/clientUploadData/saveplaylist', params, function (data, status) {
            if (data.status == 200) {
               
                toastr.success('File Uploaded Successfully');
                setTimeout(function () {
                    window.location.href = '/v1/clientUploadData/clientUploadData'
                }, 2000);
            }
        })
    }

    // getXslxdata
    $.get('/v1/clientUploadData/getXslxdata', function (data, status) {
        if (data.status == 200) {
            destroyRowsxllist()
            appendUploadedsData(data)
        }
    });

    function destroyRowsxllist() {
        $('#harexllist_tbody').empty()
        $('#harexllist_table').DataTable().rows().remove();
        $("#harexllist_table").DataTable().destroy()
    }

    function appendUploadedsData(data) {
        var array = data.data;
        if (array.length) {
            var uploaded_list = "";
            array.forEach(function (element, i) {

                var fileName = element.fileName ? element.fileName : "";
                var created_at = element.created_at ? moment(element.created_at).format('lll') : "";

                uploaded_list += `<tr class="users-tbl-row asset-row" id="${fileName}">
            <td class="">${(i + 1)}</td>
            <td class="username" key_factor="${fileName}"><a class="group-name-link"  href="#">${fileName}</a></td>
            <td class="name">${created_at}</td>
            <td class="action-td" id=${fileName}><div class="dropdown"> <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false"> <i class="fe-settings noti-icon"></i> </a> <div class="dropdown-menu dropdown-menu-right">
            <a href="#" key-value="${fileName}" class="dropdown-item delete-file">Delete</a> 
            <a href="#" key-value="${fileName}" class="dropdown-item download-file">Download</a> 
            </td>`
                if (i == array.length - 1) {
                    $('#harexllist_tbody').append(uploaded_list);
                    reInitialiClientSheetListTable()
                }
            })
        }
    }


    function reInitialiClientSheetListTable() {
        $("#harexllist_table").DataTable().destroy()
        xllist_table = $('#harexllist_table').DataTable({
            //"order": [[1, "desc"]], // for descending order
            "columnDefs": [
                { "width": "30%", "targets": 1 }
            ]
        })
        $("#harexllist_table tbody tr:first").addClass("active");
    }


    // Delete file
    $(document).on("click", ".delete-file", function (event) {

        var File_name = $(this).attr('key-value');
        let params = {
            File_name: File_name
        }

        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            confirmButtonClass: "btn btn-success mt-2",
            cancelButtonClass: "btn btn-danger ml-2 mt-2",
            buttonsStyling: !1
        }).then(function (t) {
            if (t.value) {
                $.post('/v1/clientUploadData/deletefile', params, function (data, status) {
                    if (data.status == 200) {
                        toastr.success('File Deleted');
                        setTimeout(function () {
                            window.location.reload();
                        }, 3000)
                        event.preventDefault()
                    }
                });
            }
        })
    })

    // Delete file
    $(document).on("click", ".download-file", function (event) {

        

        var File_name = $(this).attr('key-value');
      //  alert("File_name >>"+File_name)
        let params = {
            File_name: File_name
        }

        var epgFileName = File_name;
        window.location.href = `https://dev-skandha-vod.s3.ap-south-1.amazonaws.com/input-data/${epgFileName}`;
        toastr.success('File Successfully downloaded!!');

    })

   
   
})


