const mongoose = require('mongoose');

const epgfiledataSchema = mongoose.Schema({

    fileName: { type: String, required: true },
    fileData: { type: Array, required: true },
    UserName: {type: String, required: true},
    playlistFilename:{type: String, required: true},
    created_at: Date,
    updated_at: Date,
    created_by: { type: String },
}, { toJSON: { virtuals: true } });

epgfiledataSchema.pre('save', function (next) {
    if (this.isNew) {
        console.log(' IS NEW CALLED!! ');
        this.created_at = new Date();
        this.updated_at = new Date();
    } else {
        console.log(' IS NEW IS FALSE!! ');
        this.updated_at = new Date();
    }
    next();
});

module.exports = mongoose.model('userFileDataEpg', epgfiledataSchema, 'userFileDataEpg');