import requests
import xlrd
from collections import OrderedDict
import json
import sys

#example_url = 'https://dev-skandha-vod.s3.ap-south-1.amazonaws.com/input-data/WellNessSample.xlsx'
example_url = sys.argv[1]
r = requests.get(example_url)

workbook = xlrd.open_workbook(file_contents=r.content)

sh = workbook.sheet_by_index(0)

cars_list = []

for rownum in range(1, sh.nrows):
    cars = OrderedDict()
    row_values = sh.row_values(rownum)
    cars['SrNo'] = row_values[0]
    cars['ProgrammeName'] = row_values[1]
    cars['TOPIC'] = row_values[2]
    cars['Subject'] = row_values[3]
    cars['CodeNo'] = row_values[4]
    cars['Duration'] = row_values[5]
    cars_list.append(cars)

j = json.dumps(cars_list)

print(j)
# with open('data.json', 'w') as f:
#     f.write(j)