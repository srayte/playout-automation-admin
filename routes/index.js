var express = require('express');
var router = express.Router();


const cognito = require('../app/utilities/user_management/cognito');
const {spawn} = require('child_process');


var fs = require('fs');
var XLSX = require('xlsx')

router.get('/', function (req, res, next) {

 let user_id = req.session ? (req.session.user_id ? req.session.user_id : null) : null;
 

 
 
 console.log("req.session>.user_id >> " + JSON.stringify(user_id))
 if (user_id) {
  
   console.log("I SHOULD REDIREC!!")
   
   res.redirect('/v1/clients');
  
 } else {
   console.log("I SHOULD LOGIN!")
   res.render('login');
 }
});


router.get('/user-Authentication', function (req, res, next) {
  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('user/authenticateUser', { section: 'Users AuthenticateUser', sub_section: '', userData: userData, userName: userName, userRole: userRole });
})



router.get('/temp', function (req, res, next) {
  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('temp', { section: '', sub_section: '', userData: userData, userName: userName, userRole: userRole });
})

// Login
router.post('/login', (req, res, next) => {
  console.log("LOGIN POST called" + JSON.stringify(req.body))
  let params = {
    username: req.body.username,
    password: req.body.password,
  }

  cognito.Login(params).then((response) => {
    if (response.status === 200) {
      //console.log('user sign in response >> ' + JSON.stringify(response));
      req.session.user_id = "ADMIN";

      //set user details in sesssion
      req.session.userData = response.data;
      req.session.userName = response.data.idToken.payload.email;
      req.session.userRole = (response.data.idToken.payload['cognito:groups']) ? (response.data.idToken.payload['cognito:groups']) : 'User';

      console.log("USER Info >>>>>   ")
      console.log("userData>>>>>>>>>>>>>>>>>>>>   " + JSON.stringify(req.session.userData))
      console.log("userName>>>>>>>>>>>>>>>>>>>>   " + req.session.userName)
      console.log("userRole>>>>>>>>>>>>>>>>>>>>  " + req.session.userRole)


      res.json({
        status: 200,
        message: 'Login successful',
        data: response.data,
      });
    } else {
      res.json({
        status: 403,
        message: response.message,
        data: response.data,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER sign in : ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });

});


//Logout
router.get('/logout', function (req, res) {
  req.session.destroy(function (err) {
    if (err) {
      res.redirect('/');
    } else {
      req.session = null;
      console.log("Logout Success " + JSON.stringify(req.session) + " ");
      res.redirect('/');
    }
  });
});

router.get('/404', function (req, res, next) {
  console.log('BEFORE')

  setTimeout(() => {
    console.log('END OF SET TIMEOUT')
    res.render('404', { title: '404 Error' });
  }, 120000);
});

router.get('/temp', function (req, res, next) {
  res.render('temp');
});




// Login
router.post('/AuthenticateUser', (req, res, next) => {
  console.log("AuthenticateUser")

  let params  = {
   username: req.body.username,
   password: req.body.password,
   newPassword: req.body.newPassword,
   email: req.body.email
  //   password: 'Shivani@123',
  //  newPassword: 'Shivani@19',
   
}

  cognito.AuthenticateUser(params).then((response) => {
    if (response.status === 200) {
      //console.log('user sign in response >> ' + JSON.stringify(response));
    res.json({
        status: 200,
        message: 'Login successful'
      });
    } else {
      res.json({
        status: 403,
        message: response.message,
        data: response.data,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER sign in : ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });

});




// Get All Users
router.get('/getUsers', (req, res, next) => {
  console.log('***GET ALL getUsers CALLED***');


  const params = {}

  cognito.getUsers(params).then((users) => {
    res.json({
      status: 200,
      message: 'Users fetched successfully',
      data: users,
    });
  });

});






// getUserInfo
router.get('/getUserInfo/:username', (req, res, next) => {
  console.log('***getUserInfo CALLED***');

  const params = {
    username: req.params.username ? req.params.username : null
  };

  cognito.getUserInfo(params).then((user) => {
    res.json({
      status: 200,
      message: 'getUserInfo fetched successfully',
      data: user,
    });
  });

})



// registration
router.post('/register', (req, res, next) => {

  console.log("register POST called")

  const params = {
    name: req.body.name ? req.body.name : null,
    email: req.body.email ? req.body.email : null,
    password: req.body.password ? req.body.password : null
  };

  cognito.RegisterUser(params).then((response) => {
    if (response) {
      console.log('user data saved response >> ' + JSON.stringify(response));

      res.json({
        status: 200,
        message: 'User Saved Successfuly',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER SAVE: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });

});



// Delete User
router.post('/deleteUser', (req, res, next) => {
  console.log("deleteUser POST called")
  const params = {
    username: req.body.username ? req.body.username : null
  };

  cognito.deleteUser(params).then((response) => {
    if (response) {
      console.log('user delete response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'User Deleted Successfuly',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER Delete: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})



// // Get All Users
// router.get('/getBucketObjects', (req, res, next) => {
//   console.log('***GET ALL getBucketObjects CALLED***');


//   const params = {}

//   cognito.listObjects().then((data) => {
//     res.json({
//       status: 200,
//       message: 'Objects fetched successfully',
//       data: data,
//     });
//   });

// });






// // Get All Users
// router.post('/getInfoObjectspython', (req, res, next) => {
//   console.log('***GET getInfoObjectspython CALLED***');

 
//   File_name = req.body.File_name ? req.body.File_name : null
  

//   console.log("getInfoObjectspython params"+JSON.stringify(File_name));

 

//   var dataToSendjson;

//    var path=`https://dev-skandha-vod.s3.ap-south-1.amazonaws.com/${File_name}`;
//   // var filename= 'myfile'
 
//   // spawn new child process to call the python script
//   const python = spawn('python', ['script3.py',path]);
//   // collect data from script
//   python.stdout.on('data', function (data) {
//     console.log('console first ...\n'+data.toString());

//   // console.log('Pipe data from python script ...\n'+data.toString());
//    dataToSendjson = data.toString();
//    console.log("console second\n"+dataToSendjson)
//   // console.log("data"+dataToSendjson)
//   python.on('close', (code) => {
//     console.log(`child process close all stdio with code ${code}`);
//     // send data to browser
//     //res.send(dataToSendjson)
    
//     });
//    res.json({
//     status: 200,
//     message: 'filedownload fetched successfully',
//     data: dataToSendjson
//   });
  
  
//   });
//   // in close event we are sure that stream from child process is closed
 
   
// });

// var abcd = []
// router.post('/filedownload', (req, res, next) => {
//   console.log('\n\n*** GET filedownload CALLED***');

  
//    var File_name= req.body.File_name ? req.body.File_name : null


//   var dataToSend;
//   var path=`https://dev-skandha-vod.s3.ap-south-1.amazonaws.com/${File_name}`;
//   var filename= File_name
//   // spawn new child process to call the python script
//   const python = spawn('python', ['script1.py',filename,path]);
//   // collect data from script
//   python.stdout.on('data', function (data) {
//    console.log('Pipe data from python script ...\n'+data.toString());
//    dataToSend = data.toString();
//    console.log("data"+JSON.stringify(dataToSend))
//    abcd.push(dataToSend)
//   console.log("abcd"+abcd)
//   res.json({
//     status: 200,
//     message: 'filedownload fetched successfully',
//     data: dataToSend
//   });
   
//   });
//   // in close event we are sure that stream from child process is closed
//   python.on('close', (code) => {
//   console.log(`child process close all stdio with code ${code}`);
//   // send data to browser
//   //res.send(dataToSend)
  
  
//   });
   
 
  
// });







////////////////////////////////////////////////////////////////////////////////////////


module.exports = router;