var express = require('express');
var router = express.Router();

// ############################################################################ //

var fs = require('fs');
const multerS3 = require('multer-s3');
const awsConfig = require('../config/aws_config')['development'];
var multer = require('multer')
var AWS = require('aws-sdk');
var s3 = new AWS.S3();

const cognito = require('../app/utilities/user_management/cognito');

const fileData = require("../workers/fileData");
const fileDataObj = new fileData.fileDataClass();

const harekrishnaData = require("../workers/harekrishnaData");
const harekrishnaDataObj = new harekrishnaData.harekrishnaDataClass();

// const fileDataEpg = require("../workers/fileDataEpg");
// const fileDataEpgObj = new fileDataEpg.fileDataEpgClass();


const airfileData = require("../workers/airFileData");
const airfileDataObj = new airfileData.airfileDataClass();

// ############################################################################ //


router.get('/clientUploadData', function (req, res, next) {

    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('client/add_ClientData', {  section: '', sub_section: '',     userData: userData, userName: userName, userRole: userRole });
  
  })



router.get('/playlist_hare_krishna', function (req, res, next) {
    const clientName = req.session.clientName ? req.session.clientName : null;
    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('userHarekrishna/playlist_hare_krishna', { section: 'Users Listing', sub_section: '', userData: userData, userName: userName, userRole: userRole, clientName: clientName });
  })
  
  router.get('/userHarekrishna', function (req, res, next) {
      const clientName = req.session.clientName ? req.session.clientName : null;
      const userData = req.session.userData ? req.session.userData : null;
      const userName = req.session.userName ? req.session.userName : null;
      const userRole = req.session.userRole ? req.session.userRole : null;
      res.render('userHarekrishna/userHarekrishna', { section: '', sub_section: '', userData: userData, userName: userName, userRole: userRole, clientName: clientName, clientName: clientName });
  })
  
  router.get('/userHarekrishan-xlsheetdata', function (req, res, next) {
    const clientName = req.session.clientName ? req.session.clientName : null;
    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('userHarekrishna/playlist_hare_krishna', { section: '', sub_section: '', userData: userData, userName: userName, userRole: userRole,clientName: clientName });
  })
  

//var channelNameOld = '';
var fileNameInEpg = '';
router.post('/fileHare', (req, res, next) => {
  let fileNAME = '';

  const upload = multer({
    storage: multerS3({
      s3: s3,
      acl: 'public-read',
      bucket: `${awsConfig.bucket}/${awsConfig.putfolder}`,
      key: (req, file, cb) => {
        console.log(`REAL FILENAME>> ${JSON.stringify(file)}`);
      //  fileNAME = `${file.originalname}`;
      fileNAME = `${'HareNew.xlsx'}`;
        console.log(`FILE NAME>> ${fileNAME}`);
        cb(null, fileNAME);
      },
    }),
  }).array('userHarekrishna', 1);

  upload(req, res, (error) => {
    if (res) {
        console.log('VIDEO UPLOAD DONE!!!!!!');
        res.send("upload")
        var channel_name = req.body.channel_name ? req.body.channel_name : null
       // channelNameOld = channel_name;
        fileNameInEpg = fileNAME;
    } else {
        console.log(`VIDEO UPLOAD ERROR: ${error}`);
    }
  });
})

// Hare krishna data
router.post('/saveplaylist', (req, res, next) => {
  
  var fileData = req.body.fileData;
  var fileName = fileNameInEpg;
  var UserName = req.session.clientName;
  //var channelName = channelNameOld;
  const params = {
    fileData: fileData,
    fileName: 'HareNew.xlsx',
    UserName: UserName,
   // channelName : channelName
  }
  harekrishnaDataObj.save(params).then((_response) => {
    if (_response) {
      res.json({
        status: 200,
        message: 'Filedata Saved successfuly',
        data: _response,
      })
    } else {
      console.log('Filedata data save failed');
      res.json({
        status: 401,
        message: 'Filedata Save failed',
        data: null,
      })
    }
  })
})

// router.post('/saveplaylist', (req, res, next) => {
  
//     var fileData = req.body.fileData;
//     var fileName = fileNameInEpg;
//     var UserName = req.session.clientName;
//     var channelName = channelNameOld;
//     const params = {
//       fileData: fileData,
//       fileName: fileName,
//       UserName: UserName,
//       channelName : channelName
//     }
//     fileDataObj.save(params).then((_response) => {
//       if (_response) {
//         res.json({
//           status: 200,
//           message: 'Filedata Saved successfuly',
//           data: _response,
//         })
//       } else {
//         console.log('Filedata data save failed');
//         res.json({
//           status: 401,
//           message: 'Filedata Save failed',
//           data: null,
//         })
//       }
//     })
//   })

  router.post('/getharekrishna', (req, res, next) => {
    const params = {
      ID: req.body.File_Name
    }
 
    harekrishnaDataObj.findFileDataByUserName(params).then((data) => {
     // console.log(JSON.stringify(data))
      res.json({
        status: 200,
        message: 'Objects fetched successfully',
        data: data,
      });
    }).catch((err) => {
      console.log(`error: ${err}`);
      res.json({
          status: 401,
          message: 'Objects fetched failed',
          data: null,
      });
  });
  });


// Get All Users
router.get('/getXslxdata', (req, res, next) => {
    const params = {
      UserName: req.session.clientName
    }
    harekrishnaDataObj.findall(params).then((data) => {
      res.json({
        status: 200,
        message: 'Objects fetched successfully',
        data: data,
      });
    });
  });
  





  // Delete File from s3 then MongoDb
router.post('/deletefile', (req, res, next) => {

  const params = {
    File_name: req.body.File_name ? req.body.File_name : null
  }

  harekrishnaDataObj.deleteFile(params).then((data) => {
    res.json({
      status: 200,
      message: 'Delete-file successfully',
      data: data
    });
  });
  // cognito.deleteObject(params).then((data) => {
  //   if (data) {
  //     harekrishnaDataObj.deleteFile(params).then((data) => {
  //       res.json({
  //         status: 200,
  //         message: 'Delete-file successfully',
  //         data: data
  //       });
  //     });
  //   }
  // });
});
  
// // Get file data by filename
router.get('/getFileDatabyFilename', (req, res, next) => {
  
    const params = {
      fileName : req.query.fileName ? req.query.fileName : null
    }

    fileDataObj.findFileDataByFileName(params).then((data) => {
      res.json({
        status: 200,
        message: 'getFileDatabyFilename fetched successfully',
        data: data
      });
    });
  });
  

module.exports = router;