var express = require("express");
var router = express.Router();

const fileData = require("../workers/fileData");
const fileDataObj = new fileData.fileDataClass();

const epgfileData = require("../workers/epgfileData");
const epgfileDataObj = new epgfileData.epgfileDataClass();

const airfileData = require("../workers/airFileData");
const airfileDataObj = new airfileData.airfileDataClass();

const harekrishnaData = require("../workers/harekrishnaData");
const harekrishnaDataObj = new harekrishnaData.harekrishnaDataClass();

const s3m = require("../app/utilities/user_management/cognito");
const { spawn } = require("child_process");

const AWS = require("aws-sdk");

var fs = require("fs");
var s3 = new AWS.S3();
var XLSX = require("xlsx");

router.get("/", function (req, res, next) {
  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render("user/index", {
    section: "",
    sub_section: "",
    userData: userData,
    userName: userName,
    userRole: userRole,
  });
});

router.get("/UserEpgData", function (req, res, next) {
  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render("user/ClientEpgData", {
    section: "",
    sub_section: "",
    userData: userData,
    userName: userName,
    userRole: userRole,
  });
});

// // Get All Filedata
router.post("/getUserFileData", (req, res, next) => {
  const params = {
    UserName: req.body.Username ? req.body.Username : null,
  };

  fileDataObj.findFileDataByUserid(params).then((data) => {
    res.json({
      status: 200,
      message: "getUserFileData fetched successfully",
      data: data,
    });
  });
});

// // Get file data by filename
router.get("/getFileDatabyFilename", (req, res, next) => {
  const params = {
    fileName: req.query.fileName ? req.query.fileName : null,
  };
  fileDataObj.findFileDataByFileName(params).then((data) => {
    res.json({
      status: 200,
      message: "getFileDatabyFilename fetched successfully",
      data: data,
    });
  });
});

// // Get file data by filename while [.air file generation]
router.post("/getFileDatabyFilenameForOutput", (req, res, next) => {
  const params = {
    fileName: req.body.fileName ? req.body.fileName : null,
  };
  fileDataObj.findFileDataByFileName(params).then((data) => {
    res.json({
      status: 200,
      message: "getFileDatabyFilenameForOutput fetched successfully",
      data: data,
    });
  });
});

// Get All Users
router.post("/getInfoObjects", (req, res, next) => {
  File_name = req.body.File_name ? req.body.File_name : null;

  var dataToSendjson;

  var path = `https://dev-skandha-vod.s3.ap-south-1.amazonaws.com/${File_name}`;
  // var filename= 'myfile'

  // spawn new child process to call the python script
  const python = spawn("python", ["script3.py", path]);
  // collect data from script
  python.stdout.on("data", function (data) {
    dataToSendjson = data.toString();

    try {
      return res.send({
        status: 200,
        message: "filedownload fetched successfully",
        data: dataToSendjson,
      });
    } catch (err) {
      return next(err);
    }
  });
  // in close event we are sure that stream from child process is closed
  python.on("close", (code) => {
    console.log(`child process close all stdio with code ${code}`);
  });
});

var abcd = [];
router.post("/filedownload", (req, res, next) => {
  var File_name = req.body.File_name ? req.body.File_name : null;

  var dataToSend;
  var path = `https://dev-skandha-vod.s3.ap-south-1.amazonaws.com/${File_name}`;
  var filename = File_name;
  // spawn new child process to call the python script
  const python = spawn("python", ["script1.py", filename, path]);
  // collect data from script
  python.stdout.on("data", function (data) {
    dataToSend = data.toString();
    abcd.push(dataToSend);

    res.json({
      status: 200,
      message: "filedownload fetched successfully",
      data: dataToSend,
    });
  });
  // in close event we are sure that stream from child process is closed
  python.on("close", (code) => {
    console.log(`child process close all stdio with code ${code}`);
    // send data to browser
    //res.send(dataToSend)
  });
});

// // Get EPG file data by playlist filename
router.post("/getEPGFileDatabyFilename", (req, res, next) => {
  console.log("***GET ALL getEPGFileDatabyFilename CALLED***");

  const params = {
    playlistFilename: req.body.fileName ? req.body.fileName : null,
    fileName: req.body.fileName ? req.body.fileName : null,
    UserName: req.body.Username ? req.body.Username : null,
  };

  //console.log("params getEPGFileDatabyFilename"+JSON.stringify(params))

  if (params.UserName === "harekrishna") {
    fileDataObj.findFileDataByFileName(params).then((data) => {
      res.json({
        status: 200,
        message: "getFileDatabyFilename fetched successfully",
        data: data,
      });
    });
  } else {
    epgfileDataObj.findEPGbyFileName(params).then((data) => {
      res.json({
        status: 200,
        message: "getEPGFileDatabyFilename fetched successfully",
        data: data,
      });
    });
  }
});

// insert air file data into db
router.post("/airfileInsert", (req, res, next) => {
  const params = {
    airfileData: req.body.filedata,
  };
  airfileDataObj.save(params).then((data) => {
    res.json({
      status: 200,
      message: "Objects fetched successfully",
      data: data,
    });
  });
});

// // Get AIR file data by playlist filename
router.post("/getAirFile", (req, res, next) => {
  console.log("***GET ALL AIR file CALLED***");

  const params = {
    playlistFilename: req.body.fileName ? req.body.fileName : null,
  };

  // console.log("params AIR file"+JSON.stringify(params))

  airfileDataObj.findAirFilebyFileName(params).then((data) => {
    res.json({
      status: 200,
      message: "AIR file fetched successfully",
      data: data,
    });
  });
});

// // Get file data by filename
router.get("/getFileDatabyEpgFilenameforDisplay", (req, res, next) => {
  const params = {
    playlistFilename: req.query.fileName ? req.query.fileName : null,
  };

  // console.log("params EPG"+JSON.stringify(params))
  epgfileDataObj.findEPGbyFileName(params).then((data) => {
    res.json({
      status: 200,
      message: "getFileDatabyFilename fetched successfully",
      data: data,
    });
  });
});

//srmd DataUpdate
router.post("/srmdDataUpdate", (req, res, next) => {
  const params = {
    SrNo: req.body.SrNo,
    TITLE: req.body.TITLE,
    EventID: req.body.EventID,
    Channel: req.body.Channel,
    DATE: req.body.DATE,
    DURATION: req.body.DURATION,
    fileName: req.body.fileName,
  };
  fileDataObj.findOneAndUpdate(params).then((_response) => {
    if (_response) {
      res.json({
        status: 200,
        message: "srmd data Update successfuly",
        data: _response,
      });
    } else {
      console.log("srmd data Update failed");
      res.json({
        status: 401,
        message: "srmd data Update failed",
        data: null,
      });
    }
  });
});

//Wellness DataUpdate
router.post("/wellnessDataUpdate", (req, res, next) => {
  const params = {
    SrNo: req.body.SrNo,
    ProgrammeName: req.body.ProgrammeName,
    TOPIC: req.body.TOPIC,
    Subject: req.body.Subject,
    CodeNo: req.body.CodeNo,
    Duration: req.body.Duration,
    fileName: req.body.fileName,
  };

  //console.log("wellnessDataUpdate"+JSON.stringify(params))
  fileDataObj.findwellnessOneAndUpdate(params).then((_response) => {
    if (_response) {
      res.json({
        status: 200,
        message: "srmd data Update successfuly",
        data: _response,
      });
    } else {
      console.log("srmd data Update failed");
      res.json({
        status: 401,
        message: "srmd data Update failed",
        data: null,
      });
    }
  });
});

//Onetake DataUpdate
router.post("/OnetakeDataUpdate", (req, res, next) => {
  const params = {
    Name: req.body.Name,
    SrNo: req.body.SrNo,
    ContentID: req.body.ContentID,
    StartDate: req.body.StartDate,
    ProgramName: req.body.ProgramName,
    ProgramFile: req.body.ProgramFile,
    OnAirTime: req.body.OnAirTime,
    Duration: req.body.Duration,
    fileName: req.body.fileName,
  };

  // console.log("OnetakeDataUpdate"+JSON.stringify(params))
  fileDataObj.findOnetakeOneAndUpdate(params).then((_response) => {
    if (_response) {
      res.json({
        status: 200,
        message: "srmd data Update successfuly",
        data: _response,
      });
    } else {
      console.log("srmd data Update failed");
      res.json({
        status: 401,
        message: "srmd data Update failed",
        data: null,
      });
    }
  });
});

router.post("/PitaaraUpdate", (req, res, next) => {
  const params = {
    Name: req.body.Name,
    SrNo: req.body.SrNo,
    ContentID: req.body.ContentID,
    StartDate: req.body.StartDate,
    ProgramName: req.body.ProgramName,
    ProgramFile: req.body.ProgramFile,
    OnAirTime: req.body.OnAirTime,
    Duration: req.body.Duration,
    fileName: req.body.fileName,
  };

  console.log("PitaaraUpdate" + JSON.stringify(params));
  fileDataObj.findOneAndUpdatePitaara(params).then((_response) => {
    if (_response) {
      res.json({
        status: 200,
        message: "Pitaara data Update successfuly",
        data: _response,
      });
    } else {
      console.log("srmd data Update failed");
      res.json({
        status: 401,
        message: "Pitaara data Update failed",
        data: null,
      });
    }
  });
});

router.post("/GreenChilliesUpdate", (req, res, next) => {
  const params = {
    SrNo: req.body.SrNo,
    StartTime: req.body.StartTime,
    EndTime: req.body.EndTime,
    ClipId: req.body.ClipId,
    Event: req.body.Event,
    Duration: req.body.Duration,
    fileName: req.body.fileName,
  };

  //  console.log("GreenChilliesUpdate"+JSON.stringify(params))
  fileDataObj.findOneAndUpdateGreenChillies(params).then((_response) => {
    if (_response) {
      res.json({
        status: 200,
        message: "GreenChillies data Update successfuly",
        data: _response,
      });
    } else {
      console.log("GreenChillies data Update failed");
      res.json({
        status: 401,
        message: "GreenChillies data Update failed",
        data: null,
      });
    }
  });
});

// Harekrishna get fiename & Fillers
router.post("/getHarekrishnaProgramName", (req, res, next) => {
  const params = {
    ID: req.body.File_Name,
  };
  // console.log("params >> "+ JSON.stringify(params))
  harekrishnaDataObj.findFileDataByUserName(params).then((data) => {
    res.json({
      status: 200,
      message: "Objects fetched successfully",
      data: data,
    });
  });
});

// Fancode api
router.post("/FancodeUpdate", (req, res, next) => {
  const params = {
    SrNo: req.body.SrNo,
    Name: req.body.Name,
    StartDate: req.body.StartDate,
    ContentID: req.body.ContentID,
    ProgramName: req.body.ProgramName,
    Next: req.body.Next,
    ProgramFile: req.body.ProgramFile,
    InPoint: req.body.InPoint,
    OutPoint: req.body.OutPoint,
    OnAirTime: req.body.OnAirTime,
    Duration: req.body.Duration,
    fileName: req.body.fileName,
  };

  console.log("fANCODE UPDATE" + JSON.stringify(params));
  fileDataObj.findOneAndUpdateFancode(params).then((_response) => {
    if (_response) {
      res.json({
        status: 200,
        message: "Fancode data Update successfuly",
        data: _response,
      });
    } else {
      console.log("Fancode data Update failed");
      res.json({
        status: 401,
        message: "Fancode data Update failed",
        data: null,
      });
    }
  });
});

////////////////////////////////////////////////////////////////////////////////////////

module.exports = router;
