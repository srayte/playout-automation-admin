const express = require('express');
const router = express.Router();

const AWS = require('aws-sdk');
const cognito = require('../app/utilities/user_management/cognito');



router.get('/', function (req, res, next) {

    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('client/client', {  section: '', sub_section: '',     userData: userData, userName: userName, userRole: userRole });

})






// Get All Users
router.get('/listUser', (req, res, next) => {

  const params = {}

  cognito.getUsers(params).then((users) => {
    res.json({
      status: 200,
      message: 'Users fetched successfully',
      data: users,
    });
  });

});


router.post('/create_customer', (req, res, next) => {
 
  const params = {
    CustomerName:req.body.CustomerName,
    Email_ID:req.body.Email_ID
  }

  userActivityObj.save(params).then((_response) => {
    if (_response) {
        console.log('customer data saved');
        res.json({
            status: 200,
            message: 'customer Saved successfuly',
            data: _response,
        })

    } else {
        console.log('customer data save failed');
        res.json({
            status: 401,
            message: 'customer Save failed',
            data: null,
        })
    }
})
});




router.post('/add_channel', (req, res, next) => {
 
  const params = {
    FileName:req.body.file,
    Status:req.body.Status
    
  }

  ChannelListObj.save(params).then((_response) => {
    if (_response) {
        console.log('channel Name saved');
        res.json({
            status: 200,
            message: 'channel Name Saved successfuly',
            data: _response,
        })

    } else {
        console.log('channel Name save failed');
        res.json({
            status: 401,
            message: 'channel Name save failed',
            data: null,
        })
    }
})
});




// Get All Channel Data from MongoDb
router.get('/getChannelList', (req, res, next) => {

  ChannelListObj.findall().then((data) => {
      console.log("channel_listing " + JSON.stringify(data))
      if (data) {
          console.log('channels fetched successfully');
          res.json({
              status: 200,
              message: 'channels fetched successfully',
              data: data,
          })
      }
  }).catch((err) => {
      console.log(`error: ${err}`);
      res.json({
          status: 401,
          message: 'channels fetched failed',
          data: null,
      });
  });

})

module.exports = router;